/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <signal.h>
#include "procdialogs.h"
#include "favorites.h"
#include "proctable.h"
#include "callbacks.h"
//#include "prettytable.h"
#include "procactions.h"
//#include "util.h"
//#include "load-graph.h"

GtkWidget *prefs_dialog = NULL;
gint new_nice_value = 0;
int kill_signal = SIGTERM;

static void
cb_show_hide_message_toggled (GtkToggleButton *button, gpointer data)
{
	ProcData *procdata = data;
	GConfClient *client = procdata->client;
	gboolean toggle_state;
	
	toggle_state = gtk_toggle_button_get_active (button);
	gconf_client_set_bool (client, "/apps/gprocview/ghide_message", toggle_state, NULL);

}

static void
hide_dialog_button_pressed (GtkDialog *dialog, gint id, gpointer data)
{
	ProcData *procdata = data;
	
	if (id == 100) {
		add_selected_to_blacklist (procdata);
	}
	
	gtk_widget_destroy (GTK_WIDGET (dialog));
		
}

static void
kill_dialog_button_pressed (GtkDialog *dialog, gint id, gpointer data)
{
	ProcData *procdata = data;
	
	if (id == 100) 
		kill_process (procdata, kill_signal);
	
	gtk_widget_destroy (GTK_WIDGET (dialog));
		
}

void
procdialog_create_kill_dialog (ProcData *data, int signal)
{
	ProcData *procdata = data;
	GtkWidget *dialog, *label;
  	gchar *text, *title;
  	
  	kill_signal = signal;
  	
  	if (signal == SIGKILL) {
  		title = _("Kill Process");
  		text = _("_Kill Process");
  	}
  	else {
  		title = _("End Process");
  		text = _("_End Process");
  	}

	dialog = gtk_dialog_new_with_buttons (title,
                                              NULL,
                                              GTK_DIALOG_MODAL|GTK_DIALOG_DESTROY_WITH_PARENT,
                                              GTK_STOCK_CANCEL,
                                              GTK_RESPONSE_CANCEL,
				              _(text),
                                              100,
                                              NULL);

	label = gtk_label_new ("");
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG(dialog)->vbox), label,
                            TRUE, TRUE, 0);

	label = gtk_label_new ("Unsaved data will be lost.");
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG(dialog)->vbox), label,
                            TRUE, TRUE, 0);

	label = gtk_label_new ("");
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG(dialog)->vbox), label,
                            TRUE, TRUE, 0);

	gtk_dialog_set_default_response (GTK_DIALOG(dialog),GTK_RESPONSE_CANCEL);
        g_signal_connect (G_OBJECT (dialog), "response",
        		  G_CALLBACK (kill_dialog_button_pressed), procdata);

        gtk_widget_show_all (dialog);
}

static gchar *
get_nice_level (gint nice)
{

	if (nice < -7)
		return _("( Very High Priority )");
	else if (nice < -2)
		return _("( High Priority )");
	else if (nice < 3)
		return _("( Normal Priority )");
	else if (nice < 7)
		return _("( Low Priority )");
	else
		return _("( Very Low Priority)");
	
}
/*
static void
renice_scale_changed (GtkAdjustment *adj, gpointer data)
{
	GtkWidget *label = GTK_WIDGET (data);
	
	new_nice_value = adj->value;
	gtk_label_set_text (GTK_LABEL (label), get_nice_level (new_nice_value));		
	
}

static void
renice_dialog_button_pressed (GtkDialog *dialog, gint id, gpointer data)
{
	ProcData *procdata = data;
	
	if (id == 100) {
		if (new_nice_value == -100)
			return;		
		renice (procdata, -2, new_nice_value);
	}
	
	gtk_widget_destroy (GTK_WIDGET (dialog));
	renice_dialog = NULL;
}
*/
static void
prefs_dialog_button_pressed (GtkDialog *dialog, gint id, gpointer data)
{
	gtk_widget_destroy (GTK_WIDGET (dialog));
	
	prefs_dialog = NULL;
}

static void
show_tree_toggled (GtkToggleButton *button, gpointer data)
{
	ProcData *procdata = data;
	GConfClient *client = procdata->client;
	gboolean toggled;
	
	toggled = gtk_toggle_button_get_active (button);
	gconf_client_set_bool (client, "/apps/gprocview/gshow_tree", toggled, NULL);

}

static void
show_threads_toggled (GtkToggleButton *button, gpointer data)
{
	ProcData *procdata = data;
	GConfClient *client = procdata->client;
	
	gboolean toggled;
	
	toggled = gtk_toggle_button_get_active (button);
	
	gconf_client_set_bool (client, "/apps/gprocview/gshow_threads", toggled, NULL);
		
}

static void
show_kill_dialog_toggled (GtkToggleButton *button, gpointer data)
{
	ProcData *procdata = data;
	GConfClient *client = procdata->client;
	
	gboolean toggled;
	
	toggled = gtk_toggle_button_get_active (button);
	
	gconf_client_set_bool (client, "/apps/gprocview/gkill_dialog", toggled, NULL);
		
}

static void
show_hide_dialog_toggled (GtkToggleButton *button, gpointer data)
{
	ProcData *procdata = data;
	GConfClient *client = procdata->client;
	
	gboolean toggled;
	
	toggled = gtk_toggle_button_get_active (button);
	
	gconf_client_set_bool (client, "/apps/gprocview/ghide_message", toggled, NULL);
		
}
static void
proc_field_toggled (GtkToggleButton *button, gpointer data)
{
	GtkTreeViewColumn *column = data;
	gboolean toggled;
	
	toggled = gtk_toggle_button_get_active (button);

	gtk_tree_view_column_set_visible (column, toggled);
	
}

static GtkWidget *
create_proc_field_page (ProcData *procdata)
{
	GtkWidget *vbox;
	GtkWidget *tree = procdata->tree;
	GList *columns = NULL;
	GtkWidget *check_button;
	
	vbox = gtk_vbox_new (TRUE, GNOME_PAD_SMALL);
	
	columns = gtk_tree_view_get_columns (GTK_TREE_VIEW (tree));
	
	while (columns) {
		GtkTreeViewColumn *column = columns->data;
		const gchar *title;
		gboolean visible;
		
		title = gtk_tree_view_column_get_title (column);
		if (!title) 
			title = _("Icon");
		
		visible = gtk_tree_view_column_get_visible (column);
		
		check_button = gtk_check_button_new_with_label (title);
		
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check_button), 
				              visible);
		g_signal_connect (G_OBJECT (check_button), "toggled",
			          G_CALLBACK (proc_field_toggled), column);
			    
		gtk_box_pack_start (GTK_BOX (vbox), check_button, FALSE, FALSE, 0);
		
		columns = g_list_next (columns);
	}
		
	
	return vbox;
}
static void
entry_activate_cb (GtkEntry *entry, gpointer data)
{
	GtkDialog *dialog = GTK_DIALOG (data);
	
	gtk_dialog_response (dialog, 100);
	
}	

/*
** type determines whether if dialog is for killing process (type=0) or renice (type=other).
** extra_value is not used for killing and is priority for renice
*/
void procdialog_create_root_password_dialog (gint type, ProcData *procdata, gint pid, 
					     gint extra_value, gchar *text)
{
	GtkWidget *dialog;
	GtkWidget *error_dialog;
	GtkWidget *main_vbox;
	GtkWidget *hbox;
	GtkWidget *entry;
	GtkWidget *label;
	gchar *title = NULL, *button_label;
	gchar *command;
	gchar *password, *blank;
	gint retval;

	if (type == 0) {
		if (extra_value == SIGKILL) {
			title = g_strdup (_("Kill Process"));
			button_label = g_strdup (_("_Kill Process"));
		}
		else {
			title = g_strdup (_("End Process"));
			button_label = g_strdup (_("_End Process"));
		}
	}
	else {
		title = g_strdup (_("Change Priority"));
		button_label = g_strdup (_("Change _Priority"));
	}
		
	dialog = gtk_dialog_new_with_buttons (title, NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      button_label, 100,
					      NULL);
	
	main_vbox = GTK_DIALOG (dialog)->vbox;
	
	label = gtk_label_new (_(text));
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_misc_set_padding (GTK_MISC (label), GNOME_PAD, 2 * GNOME_PAD);
	gtk_box_pack_start (GTK_BOX (main_vbox), label, FALSE, FALSE, 0);
	
	hbox = gtk_hbox_new (FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), GNOME_PAD_SMALL);
	gtk_box_pack_start (GTK_BOX (main_vbox), hbox, FALSE, FALSE, 0);
	
	label = gtk_label_new (_("Root Password :"));
	gtk_misc_set_padding (GTK_MISC (label), GNOME_PAD_SMALL, 0);
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, FALSE, 0);
	
	entry = gtk_entry_new ();
	gtk_entry_set_visibility (GTK_ENTRY (entry), FALSE);
	gtk_box_pack_start (GTK_BOX (hbox), entry, TRUE, FALSE, 0);
	g_signal_connect (G_OBJECT (entry), "activate",
			  G_CALLBACK (entry_activate_cb), dialog);
		
	gtk_widget_show_all (main_vbox);
	
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_CANCEL);
	gtk_widget_grab_focus (entry);
		
	g_free (title);	
	g_free (button_label);
	
	retval = gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_hide (dialog);
	
	if (retval == 100) {
		password = gtk_editable_get_chars (GTK_EDITABLE (entry), 0, -1);
		
		if (!password)
			password = "";
		blank = g_strdup (password);
		if (strlen (blank))
			memset (blank, ' ', strlen (blank));
			
		gtk_entry_set_text (GTK_ENTRY (entry), blank);
		gtk_entry_set_text (GTK_ENTRY (entry), "");
		g_free (blank);
		
		if (type == 0)
			command = g_strdup_printf ("kill -s %d %d", extra_value, pid);
		else
			command = g_strdup_printf ("renice %d %d", extra_value, pid);
			
	/*	if (su_run_with_password (command, password) == -1) {
			error_dialog = gtk_message_dialog_new (NULL,
							       GTK_DIALOG_DESTROY_WITH_PARENT,
                                  			       GTK_MESSAGE_ERROR,
                                  			       GTK_BUTTONS_OK,
                                  			       "%s",
                                  			      _("Wrong Password."),
                                  			      NULL); 
			gtk_dialog_run (GTK_DIALOG (error_dialog));
			gtk_widget_destroy (error_dialog);
		}*/
		g_free (command);
		
	}
	gtk_widget_destroy (dialog);
}

