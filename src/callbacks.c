/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <signal.h>
#include <gtk/gtkwidget.h>
#include "callbacks.h"
#include "interface.h"
#include "proctable.h"
//#include "infoview.h"
#include "procactions.h"
#include "procdialogs.h"
#include "favorites.h"
extern GtkWidget *app;
extern pid_t cpid;
extern ProcData *procdata;
gboolean isStopSamplingSelected = FALSE;
gchar *selected_string; 
//extern gboolean WriteRecords;
//extern gboolean Process_Executing;
gboolean islogvisible = FALSE;
gchar *logfilename;

void Func_Writer ();
gboolean isCompleteCycleOver = FALSE; 
gboolean isSignalPending = FALSE;
extern gboolean User_Selection_Changed;
static void
get_selected_iter (GtkTreeModel *model, GtkTreePath *path,
                   GtkTreeIter *iter, gpointer data);
GtkMenuItem     *logmenuitem;
void change_caption ()
{
        GtkWidget *label = GTK_BIN (logmenuitem)->child;
        G_CONST_RETURN gchar *menu_caption ;
        menu_caption = gtk_label_get_text (GTK_LABEL(label));
        if (g_ascii_strcasecmp (menu_caption, "Log Visible...") == 0)
        {
                islogvisible = TRUE;
                gtk_label_set_text (GTK_LABEL(label), "Stop Logging");
        }
        else
        {
                islogvisible = FALSE;
                gtk_label_set_text (GTK_LABEL(label), "Log Visible...");
                g_free (logfilename); logfilename = NULL;
        }
}

gchar *
get_size_string (gfloat fsize)
{
 
        if (fsize < 1024.0)
                return g_strdup_printf (_("%d bytes"), (int)fsize);
        fsize /= 1024.0;
        if (fsize < 1024.0)
                return g_strdup_printf (_("%d K"), (int)fsize);
 
        fsize /= 1024.0;
        if (fsize < 100.0)
                return g_strdup_printf (_("%.1f MB"), fsize);
        else if (fsize < 1024.0)
                return g_strdup_printf (_("%.0f MB"), fsize);
 
        fsize /= 1024.0;
        return g_strdup_printf (_("%.1f GB"), fsize);
 
}                                                                          
void cb_show_filed_clicked (GtkMenuItem *menuitem, gpointer user_data)
{
	//GtkCheckMenuItem* item = GTK_CHECK_MENU_ITEM (widget);
	ProcData *procdata = user_data;
        GtkWidget *tree = procdata->tree;
        GList *columns = NULL;
	GtkTreeViewColumn *column;	 
        columns = gtk_tree_view_get_columns (GTK_TREE_VIEW (tree));
/* 
        for (i = 0; i < ; i++)
	{         
		column = columns->data;
                columns = g_list_next (columns);
        } 
        gboolean toggled;
 
        toggled = gtk_toggle_button_get_active (button);
 
        gtk_tree_view_column_set_visible (column, toggled); 
*/
}

void cb_select_all (GtkMenuItem *menuitem, gpointer user_data)
{
 	 ProcData *procdata = user_data;
	GtkTreeSelection *selection;
	GtkWidget *tree = procdata->tree;
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree));	
	gtk_tree_selection_select_all (selection);
}
 
void
cb_sort_clicked_id (GtkMenuItem *menuitem, gpointer user_data)
{
        ProcData *procdata = user_data;
	gint col_index = 0;
        GList *columns = NULL;
        GtkWidget *tree = procdata->tree;
	GtkSortType sort_type;
	GtkTreeModel *model = gtk_tree_view_get_model (GTK_TREE_VIEW (tree));
        GtkTreeViewColumn *column = NULL;
	gint col_num;

        columns = gtk_tree_view_get_columns (GTK_TREE_VIEW (tree));

	col_index = 0;
        do  {
                columns = g_list_next (columns);
                col_index++;
        } while (col_index < COL_PID);

        column = columns->data;

	gtk_tree_sortable_get_sort_column_id (GTK_TREE_SORTABLE (model),
                                              &col_num,
                                              &sort_type);

	if (sort_type == GTK_SORT_ASCENDING)
	{
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
                                              COL_PID,
                                              GTK_SORT_DESCENDING);
	}
	else {
	
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
                                              COL_PID,
                                              GTK_SORT_ASCENDING);
	}
}

void
cb_sort_clicked_name (GtkMenuItem *menuitem, gpointer user_data)
{
        ProcData *procdata = user_data;
        gint col_index = 0;
        GList *columns = NULL;
	gboolean sort_flag =FALSE ;
        GtkWidget *tree = procdata->tree;
	GtkSortType sort_type;
	GtkTreeModel *model = gtk_tree_view_get_model (GTK_TREE_VIEW (tree));
        GtkTreeViewColumn *column = NULL;
        columns = gtk_tree_view_get_columns (GTK_TREE_VIEW (tree));
	col_index = 0;
        do  {
                columns = g_list_next (columns);
                col_index++;
        } while (col_index < COL_NAME);

        column = columns->data;
	sort_flag = gtk_tree_sortable_get_sort_column_id
                                              (GTK_TREE_SORTABLE (model),
                                               &col_index,
                                               &sort_type);
	if (sort_type ==GTK_SORT_ASCENDING)
	{
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
                                              COL_NAME,
                                              GTK_SORT_DESCENDING);
		
	}
	else
	{
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
                                              COL_NAME,
                                              GTK_SORT_ASCENDING);
	}
}

void
cb_sort_clicked_owner (GtkMenuItem *menuitem, gpointer user_data)
{
        ProcData *procdata = user_data;
        gint col_index = 0;
        GList *columns = NULL;
        GtkWidget *tree = procdata->tree;
	GtkSortType sort_type;
	GtkTreeModel *model = gtk_tree_view_get_model (GTK_TREE_VIEW (tree));
        GtkTreeViewColumn *column = NULL;
        columns = gtk_tree_view_get_columns (GTK_TREE_VIEW (tree));
        col_index = 0;
        do  {
                columns = g_list_next (columns);
                col_index++;
        } while (col_index < COL_USER);

        column = columns->data;
	gtk_tree_sortable_get_sort_column_id (GTK_TREE_SORTABLE (model),
                                              &col_index,
                                              &sort_type);
	if (sort_type ==GTK_SORT_ASCENDING)
	{
		gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
                                              COL_USER,
                                              GTK_SORT_DESCENDING);		
	}
	else
	{
		gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
                                              COL_USER,
                                              GTK_SORT_ASCENDING);
	}	
}

void
cb_sort_clicked_cpu (GtkMenuItem *menuitem, gpointer user_data)
{
        ProcData *procdata = user_data;
        gint col_index = 0;
        GList *columns = NULL;
	GtkSortType sort_type;
        GtkWidget *tree = procdata->tree;
	GtkTreeModel *model = gtk_tree_view_get_model (GTK_TREE_VIEW (tree));
        GtkTreeViewColumn *column = NULL;
        columns = gtk_tree_view_get_columns (GTK_TREE_VIEW (tree));
        col_index = 0;
        do  {
                columns = g_list_next (columns);
                col_index++;
        } while (col_index < COL_CPU);

        column = columns->data;
	gtk_tree_sortable_get_sort_column_id (GTK_TREE_SORTABLE (model),
                                              &col_index,
                                              &sort_type);
	if (sort_type == GTK_SORT_ASCENDING)
	{
		
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
                                              COL_CPU,
                                              GTK_SORT_DESCENDING);
	}
	else
	{	
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
                                              COL_CPU,
                                              GTK_SORT_ASCENDING);
	}
}

void
cb_sort_clicked_mem (GtkMenuItem *menuitem, gpointer user_data)
{
        ProcData *procdata = user_data;
        gint col_index = 0;
        GList *columns = NULL;
        GtkWidget *tree = procdata->tree;
	GtkSortType sort_type;
	GtkTreeModel *model = gtk_tree_view_get_model (GTK_TREE_VIEW (tree));
        GtkTreeViewColumn *column = NULL;
        columns = gtk_tree_view_get_columns (GTK_TREE_VIEW (tree));
        col_index = 0;
        do  {
                columns = g_list_next (columns);
                col_index++;
        } while (col_index < COL_MEM);

        column = columns->data;
        //gtk_tree_view_column_set_sort_indicator (column, TRUE);
	gtk_tree_sortable_get_sort_column_id (GTK_TREE_SORTABLE (model),
                                              &col_index,
                                              &sort_type);
	if (sort_type == GTK_SORT_ASCENDING)
	{
	
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
                                              COL_MEM,
                                              GTK_SORT_DESCENDING);
	}
	else
	{
	
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
                                              COL_MEM,
                                              GTK_SORT_ASCENDING);
	}
}

void
cb_sort_clicked_swap (GtkMenuItem *menuitem, gpointer user_data)
{
        ProcData *procdata = user_data;
        gint col_index = 0;
        GList *columns = NULL;
	GtkSortType sort_type;
        GtkWidget *tree = procdata->tree;
	GtkTreeModel *model = gtk_tree_view_get_model (GTK_TREE_VIEW (tree));
        GtkTreeViewColumn *column = NULL;
        columns = gtk_tree_view_get_columns (GTK_TREE_VIEW (tree));
        col_index = 0;
        do  {
                columns = g_list_next (columns);
                col_index++;
        } while (col_index < COL_VMSIZE);

        column = columns->data;
	gtk_tree_sortable_get_sort_column_id (GTK_TREE_SORTABLE (model),
                                              &col_index,
                                              &sort_type);
	if (sort_type == GTK_SORT_ASCENDING)
	{
	
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
                                              COL_VMSIZE,
                                              GTK_SORT_DESCENDING);
	}
	else
       {

        gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
                                              COL_VMSIZE,
                                              GTK_SORT_ASCENDING);
        }
}

void
cb_sort_clicked_time (GtkMenuItem *menuitem, gpointer user_data)
{
        ProcData *procdata = user_data;
        gint col_index = 0;
        GList *columns = NULL;
        GtkWidget *tree = procdata->tree;
	GtkSortType sort_type;
	GtkTreeModel *model = gtk_tree_view_get_model (GTK_TREE_VIEW (tree));
        GtkTreeViewColumn *column = NULL;
        columns = gtk_tree_view_get_columns (GTK_TREE_VIEW (tree));
        col_index = 0;
        do  {
                columns = g_list_next (columns);
                col_index++;
        } while (col_index < COL_STARTED);

        column = columns->data;
        //gtk_tree_view_column_set_sort_indicator (column, TRUE);
	gtk_tree_sortable_get_sort_column_id (GTK_TREE_SORTABLE (model),
                                              &col_index,
                                              &sort_type);
	if (sort_type == GTK_SORT_ASCENDING)
	{
		
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
                                              COL_STARTED,
                                              GTK_SORT_DESCENDING);
	}
	else
	{

        gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
                                              COL_STARTED,
                                              GTK_SORT_ASCENDING);
	}
}
void
cb_sort_clicked_parent (GtkMenuItem *menuitem, gpointer user_data)
{
        ProcData *procdata = user_data;
        gint col_index = 0;
        GList *columns = NULL;
        GtkWidget *tree = procdata->tree;
	GtkSortType sort_type;
	GtkTreeModel *model = gtk_tree_view_get_model (GTK_TREE_VIEW (tree));
        GtkTreeViewColumn *column = NULL;
        columns = gtk_tree_view_get_columns (GTK_TREE_VIEW (tree));
        col_index = 0;
        do  {
                columns = g_list_next (columns);
                col_index++;
        } while (col_index < COL_PPID);

        column = columns->data;
        //gtk_tree_view_column_set_sort_indicator (column, TRUE);
	gtk_tree_sortable_get_sort_column_id (GTK_TREE_SORTABLE (model),
                                              &col_index,
                                              &sort_type);
	if (sort_type == GTK_SORT_ASCENDING)
	{
	
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
                                              COL_PPID,
                                              GTK_SORT_DESCENDING);
	}
	else
	{
	
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
                                              COL_PPID,
                                              GTK_SORT_ASCENDING);
	}

}

void
cb_sort_clicked_cmd (GtkMenuItem *menuitem, gpointer user_data)
{
        ProcData *procdata = user_data;
        gint col_index = 0;
        GList *columns = NULL;
        GtkWidget *tree = procdata->tree;
	GtkSortType sort_type;
	GtkTreeModel *model = gtk_tree_view_get_model (GTK_TREE_VIEW (tree));
        GtkTreeViewColumn *column = NULL;
        columns = gtk_tree_view_get_columns (GTK_TREE_VIEW (tree));
        col_index = 0;
        do  {
                columns = g_list_next (columns);
                col_index++;
        } while (col_index < COL_ARGS);

        column = columns->data;
        //gtk_tree_view_column_set_sort_indicator (column, TRUE);
	gtk_tree_sortable_get_sort_column_id (GTK_TREE_SORTABLE (model),
                                              &col_index,
                                              &sort_type);
	if (sort_type == GTK_SORT_ASCENDING)
	{	

	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
                                              COL_ARGS,
                                              GTK_SORT_DESCENDING);
	}
	else
	{
	
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
                                              COL_ARGS,
                                              GTK_SORT_ASCENDING);
	}
}

void
cb_owner (GtkMenuItem *menuitem, gpointer user_data)
{
         ProcData *procdata = user_data;
}

void
cb_debug (GtkMenuItem *menuitem, gpointer user_data)
{
         ProcData *procdata = user_data;
	 debug_process (procdata);
}

void
cb_trce_children (GtkMenuItem *menuitem, gpointer user_data)
{
         ProcData *procdata = user_data;
	 trace_children_dialog (procdata);
}

void 
cb_show_all_processes (GtkMenuItem *menuitem, gpointer user_data)
{
	 ProcData *procdata = user_data;
	create_blacklist_dialog (procdata);
}

void
cb_stop_sampling(GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	GtkWidget *widget = GTK_WIDGET (menuitem);
        //ProcData *procdata = user_data;	
	GtkWidget *label;
	gchar *tempchar;
	label = GTK_BIN(menuitem)->child;
	if (isStopSamplingSelected)
	{
		isStopSamplingSelected = FALSE;
		gtk_label_set_text(GTK_LABEL(label), "Stop Sampling");

                procdata->timeout = gtk_timeout_add (procdata->config.update_interval,  cb_timeout, procdata);
		update_sensitivity_process (TRUE);	
	}

	else
	{
		isStopSamplingSelected = TRUE;
		gtk_label_set_text(GTK_LABEL(label), "Start Sampling");

        	gtk_timeout_remove (procdata->timeout);
		update_sensitivity_process (FALSE);
	}
}  

void
cb_sample_now (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	if (isSignalPending == FALSE)
	{	
		isSignalPending = TRUE;
		if (isCompleteCycleOver == FALSE)
			while (isCompleteCycleOver == FALSE);

		if (isCompleteCycleOver == TRUE)
		{
			isCompleteCycleOver = FALSE;
			proctable_clear_tree (procdata);
			update_sensitivity_controls (FALSE);
                	User_Selection_Changed = TRUE;
			Writer_Func (); 
			Reader_Func ();
		}
	}

}   

void
cb_hide_selected_process (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
        ProcData *procdata = user_data;
} 

void
cb_show_sort_by (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
        ProcData *procdata = user_data;
}  

void
cb_show_columns (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
        ProcData *procdata = user_data;
}  

void
cb_toolbar_clicked (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
        ProcData *procdata = user_data;
} 

void
cb_copy_clicked (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	ProcData *procdata = user_data;
	GtkTreeSelection *selection;
	GtkTreeIter iter;
	GtkTreeModel *model;
	GValue value = {0, };
	char tempbuffer [1024];
	GtkWidget *tree = procdata->tree;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree));
	gtk_tree_selection_selected_foreach (selection, get_selected_iter,
                                            selected_string);


	if (selected_string != NULL)  {
		gtk_clipboard_set_text(gtk_clipboard_get (GDK_SELECTION_CLIPBOARD),
                               selected_string,
                               strlen (selected_string));
		g_free (selected_string);
		selected_string = NULL;
	}
} 

void
cb_show_ancestry (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
        ProcData *procdata = user_data;
	show_ancestry_dialog (procdata);
} 

void
cb_show_stack (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
        ProcData *procdata = user_data;
	show_stack_dialog (procdata);
} 
	
void
cb_trace_children (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
        ProcData *procdata = user_data;
	 trace_children_dialog (procdata);
} 

gint dump_log_in_file ()
{
	FILE *fileptr = fopen (logfilename, "a");
	char tempbuffer [MAX_TEMP_BUFFER_SIZE];
	GList *list = procdata->info;
	ProcInfo *info;
	fprintf (fileptr, "  ID    Name     User    CPU%  RAM   Swap   Status   Parent ID  Command \n");
	while (list != NULL)
	{
         	info = list->data;
	 	sprintf (tempbuffer, "%5d %10s %10s %3.2f %5d %5d %15s %5d %20s \n",
		info->pid,
		info->name,
		info->user,
		info->cpu,
		info->mem,
		info->vmsize,
		info->status,
		info->parent_pid,
		info->arguments);
		 fprintf (fileptr, tempbuffer);
		 list = g_list_next(list);
	}
	fclose (fileptr);
}


static void save_log_file (GtkWidget *w, GtkFileSelection *fs) 
{
	if  (islogvisible == FALSE)
	{
		FILE *fileptr = NULL;	
         	logfilename = g_strdup (gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs)));
		fileptr = fopen (logfilename, "w");
                if (fileptr == NULL)
                {
                        gchar *error = "Not a Valid Log FileName ";
                        GtkWidget *dialog = gtk_message_dialog_new (NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
                                                 GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
                                                 "%s", error, NULL);
                        gtk_dialog_run (GTK_DIALOG (dialog));
                        gtk_widget_destroy (dialog);
                        return;
                }
	 	if (fileptr)
			fclose (fileptr);	

                islogvisible = TRUE ;
                change_caption ();
		gtk_widget_destroy (GTK_WIDGET(fs));
	}
}
void file_ok_sel( GtkWidget *w, GtkFileSelection *fs )
{
	
	FILE *fileptr = NULL;
	G_CONST_RETURN gchar *FileName = NULL;
	char tempbuffer [MAX_TEMP_BUFFER_SIZE];
	GList *list = procdata->info;
	ProcInfo *info;

	FileName = gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs)); 	

	fileptr = fopen (FileName, "w");
        if (fileptr == NULL)
        {
                gchar *error = "Not a Valid FileName ";
                GtkWidget *dialog = gtk_message_dialog_new (NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
                                                 GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
                                                 "%s", error, NULL);
                gtk_dialog_run (GTK_DIALOG (dialog));
                gtk_widget_destroy (dialog);
                return;

        }
	while (list != NULL)
	{
		info = list->data;
		sprintf (tempbuffer, "%5d %10s %10s %3.2f %5d %5d %15s %5d %20s \n",
					info->pid,
					info->name,
					info->user,
					info->cpu,
					info->mem, 	
					info->vmsize,
					info->status,
					info->parent_pid,
					info->arguments);
		fprintf (fileptr, tempbuffer); 
		list = g_list_next(list);
	}
	fclose (fileptr);
	gtk_widget_destroy (GTK_WIDGET(fs));	
	logfilename = NULL;
}

static void
get_selected_iter (GtkTreeModel *model, GtkTreePath *path,
                   GtkTreeIter *iter, gpointer data)
{
        ProcInfo *info;
        GValue pid_value = {0, };
        GValue name_value = {0, };
        GValue user_value = {0, };
        GValue cpu_value = {0, };
        GValue mem_value = {0, };
        GValue vmsize_value = {0, };
        GValue status_value = {0, };
        GValue parent_value = {0, };
        GValue args_value = {0, };
        gchar *tmp_name = NULL;
        char tempbuffer [1024] ;
        gchar *tmp_selected_string = NULL;

        gtk_tree_model_get (model, iter, COL_POINTER, &info, -1);

        /* Extract values from the iter.*/
        gtk_tree_model_get_value (model, iter, COL_PID, &pid_value);
        gtk_tree_model_get_value (model, iter, COL_NAME, &name_value);
        gtk_tree_model_get_value (model, iter, COL_USER, &user_value);
        gtk_tree_model_get_value (model, iter, COL_CPU, &cpu_value);
        gtk_tree_model_get_value (model, iter, COL_MEM, &mem_value);
        gtk_tree_model_get_value (model, iter, COL_VMSIZE, &vmsize_value);
        gtk_tree_model_get_value (model, iter, COL_STARTED, &status_value);
        gtk_tree_model_get_value (model, iter, COL_PPID, &parent_value);
        gtk_tree_model_get_value (model, iter, COL_ARGS, &args_value);
        sprintf (tempbuffer, "%d %s %s %f %d %d %s %d %s",
				g_value_get_int (&pid_value),
                                g_value_get_string (&name_value),
                                g_value_get_string (&user_value),
                                g_value_get_float (&cpu_value),
                                g_value_get_int (&mem_value),
                                g_value_get_int (&vmsize_value),
                                g_value_get_string (&status_value),
                                g_value_get_int (&parent_value),
                                g_value_get_string (&args_value));

        if (!selected_string) {
		selected_string = g_strdup (tempbuffer);
        }
        else {
		tmp_selected_string = selected_string;
		selected_string = g_strjoin ("\n", tmp_selected_string, 
					     tempbuffer, NULL);
		g_free (tmp_selected_string);
        }

        g_return_if_fail (info);

        g_value_unset (&pid_value);
        g_value_unset (&name_value);
        g_value_unset (&user_value);
        g_value_unset (&cpu_value);
        g_value_unset (&mem_value);
        g_value_unset (&vmsize_value);
        g_value_unset (&status_value);
        g_value_unset (&parent_value);
        g_value_unset (&args_value);
}

void
cb_save_visible (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	ProcData *procdata = user_data;
   	GtkWidget *filew;
	//gtk_tree_selection_selected_foreach (procdata->selection, get_selected_iter,
        //                                     procdata);                          
       /* Create a new file selection widget */
       filew = gtk_file_selection_new ("Gprocview Save Visible");
       gtk_window_set_modal (GTK_WINDOW (filew), TRUE); 	                     
       g_signal_connect (G_OBJECT (filew), "destroy",
       		G_CALLBACK (gtk_widget_destroy), NULL);
       /* Connect the ok_button to file_ok_sel function */
       g_signal_connect (G_OBJECT (GTK_FILE_SELECTION (filew)->ok_button),
       				"clicked", 
                                G_CALLBACK (file_ok_sel), filew);
                             
       /* Connect the cancel_button to destroy the widget */
       g_signal_connect_swapped (G_OBJECT (GTK_FILE_SELECTION (filew)->cancel_button),
       				"clicked", 
                                 G_CALLBACK (gtk_widget_destroy), filew);
                             
      /* Lets set the filename, as if this were a save dialog, and we are giving
      a default filename */
      gtk_file_selection_set_filename (GTK_FILE_SELECTION(filew), 
      "gproc");
                             
      gtk_widget_show (filew);

}    
static void cb_log_cancel (GtkWidget *w, GtkFileSelection *fs)
{
        islogvisible == FALSE;
        gtk_widget_destroy (GTK_WIDGET(w));
}

void
cb_log_visible (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
        ProcData *procdata = user_data;                                                
	GtkWidget *label = GTK_BIN (menuitem)->child;
	G_CONST_RETURN gchar *menu_caption ;
	GtkWidget *logfile;
	logmenuitem = menuitem;	
	if (islogvisible == FALSE)
	{
		/* Create a new file selection widget */
		logfile = gtk_file_selection_new ("Gprocview Log Visible");
		gtk_window_set_modal (GTK_WINDOW (logfile), TRUE);
		g_signal_connect (G_OBJECT (logfile), "destroy",
			G_CALLBACK (gtk_widget_destroy), NULL);
		/* Connect the ok_button to file_ok_sel function */
		g_signal_connect (G_OBJECT (GTK_FILE_SELECTION (logfile)->ok_button),
				"clicked", G_CALLBACK (save_log_file), logfile);

		/* Connect the cancel_button to destroy the widget */
		g_signal_connect_swapped (G_OBJECT (GTK_FILE_SELECTION (logfile)->cancel_button),
				"clicked", G_CALLBACK (cb_log_cancel), logfile);

		/* Lets set the filename, as if this were a save dialog, and we are giving
		a default filename */
		gtk_file_selection_set_filename (GTK_FILE_SELECTION(logfile), "log");

		gtk_widget_show (logfile);
	}
        else
        {
                islogvisible = FALSE;
                change_caption ();
        }
}

void
cb_send_signal (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
        ProcData *procdata = user_data;
	send_signal_dialog (procdata);
} 

void
cb_trace_system_call (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
        ProcData *procdata = user_data;
	trace_system_call (procdata);
} 


void
cb_end_process (GtkMenuItem *menuitem, gpointer data)
{
	ProcData *procdata = data;
	
	if (procdata->config.show_kill_warning)
		procdialog_create_kill_dialog (procdata, SIGTERM);
	else
		kill_process (procdata, SIGTERM);
	
}

void
cb_kill_process (GtkMenuItem *menuitem, gpointer data)
{
	ProcData *procdata = data;
	
	if (procdata->config.show_kill_warning)
		procdialog_create_kill_dialog (procdata, SIGKILL);
	else
		kill_process (procdata, SIGKILL);
	
}

void		
cb_show_hidden_processes (GtkMenuItem *menuitem, gpointer data)
{
	ProcData *procdata = data;
	
	create_blacklist_dialog (procdata);
}

void
cb_hide_process (GtkMenuItem *menuitem, gpointer data)
{
	ProcData *procdata = data;
	add_selected_to_blacklist (procdata);
	update_hide_process_menu(procdata);	
}


void
cb_about_activate (GtkMenuItem *menuitem, gpointer user_data)
{

	GtkWidget *about;
	const gchar *authors[] = {
				 _("Nitin Yewale (nitin.yewale@wipro.com)"),
				 NULL
				 };
				 
	about = gnome_about_new (_("GProcView"), VERSION,
				 _("(C) 2002 "),
				 _("Gnome process viewer "),
				 authors,
				 NULL, NULL, NULL);
				 
	gtk_widget_show (about);  
	
}


void
cb_app_exit (GtkObject *object, gpointer user_data)
{
	ProcData *procdata = user_data;
	
	cb_app_delete (NULL, NULL, procdata);
	
}

void		
cb_app_delete (GtkWidget *window, GdkEventAny *ev, gpointer data)
{
	ProcData *procdata = data;
	
	gprocview_save_config (procdata);
	if (procdata->timeout != -1)
		gtk_timeout_remove (procdata->timeout);
	if (procdata->disk_timeout != -1)
		gtk_timeout_remove (procdata->disk_timeout);
		
	gtk_main_quit ();
	
}
#if 0
gboolean	
cb_close_simple_dialog (GnomeDialog *dialog, gpointer data)
{
	ProcData *procdata = data;
	
	if (procdata->timeout != -1)
		gtk_timeout_remove (procdata->timeout);
	
	gtk_main_quit ();
		
	return FALSE;

}
#endif
void
cb_all_process_menu_clicked 		(GtkWidget	*widget,
					 gpointer	data)
{
	ProcData *procdata = data;
	GConfClient *client = procdata->client;
	
	g_return_if_fail (data);
	if (procdata->config.whose_process != ALL_PROCESSES)
	{
		procdata->config.whose_process = ALL_PROCESSES;
		gconf_client_set_int (client, "/apps/gprocview/gview_as", 
			      procdata->config.whose_process, NULL);
		
		if (isSignalPending == FALSE)
		{
			isSignalPending = TRUE;
			if (isCompleteCycleOver == FALSE)
				while (isCompleteCycleOver == FALSE);
			if (isCompleteCycleOver == TRUE)
			{
	 			update_sensitivity_controls (FALSE);	
				isCompleteCycleOver = FALSE;
				proctable_clear_tree (procdata);
                		User_Selection_Changed = TRUE;
				Writer_Func ();
				Reader_Func ();
			}
		}
	}
	
}


void
cb_my_process_menu_clicked		(GtkWidget	*widget,
					 gpointer	data)
{
	ProcData *procdata = data;
	GConfClient *client = procdata->client;
	
	g_return_if_fail (data);
	if (procdata->config.whose_process != MY_PROCESSES)
	{
		procdata->config.whose_process = MY_PROCESSES;
		gconf_client_set_int (client, "/apps/gprocview/gview_as", 
			      procdata->config.whose_process, NULL);
		
		if (isSignalPending == FALSE)
		{	
			isSignalPending = TRUE;
	

			if (isCompleteCycleOver == FALSE)
				while (isCompleteCycleOver == FALSE);

			if (isCompleteCycleOver == TRUE)
			{	
				isCompleteCycleOver = FALSE;
	 			update_sensitivity_controls (FALSE);	
				proctable_clear_tree (procdata);
                		User_Selection_Changed = TRUE;
				Writer_Func ();
				Reader_Func ();
			}
		}
	}
}

void 
popup_menu_end_process (GtkMenuItem *menuitem, gpointer data)
{
	ProcData *procdata = data;

        if (procdata->config.show_kill_warning)
		procdialog_create_kill_dialog (procdata, SIGTERM);
	else
		kill_process (procdata, SIGTERM);	
}

void 
popup_menu_kill_process (GtkMenuItem *menuitem, gpointer data)
{
	ProcData *procdata = data;

        if (procdata->config.show_kill_warning)
		procdialog_create_kill_dialog (procdata, SIGKILL);
	else	
		kill_process (procdata, SIGKILL);
			
}
#if 0
void 
popup_menu_about_process (GtkMenuItem *menuitem, gpointer data)
{
	ProcData *procdata = data;
	ProcInfo *info = NULL;
	gchar *name;
	
	if (!procdata->selected_node)
		return;
		
	info = e_tree_memory_node_get_data (procdata->memory, 
					    procdata->selected_node);
	g_return_if_fail (info != NULL);
	
	/* FIXME: this is lame. GNOME help browser sucks balls. There should be a way
	to first check man pages, then info pages and give a nice error message if nothing
	exists */			    
	name = g_strjoin (NULL, "man:", info->cmd, NULL);
	gnome_url_show (name);
	g_free (name);
					    
}
#endif

void
cb_end_process_button_pressed          (GtkButton       *button,
                                        gpointer         data)
{

	ProcData *procdata = data;

	if (procdata->config.show_kill_warning)
		procdialog_create_kill_dialog (procdata, SIGTERM);
	else
		kill_process (procdata, SIGTERM);
	
}

void
cb_info_button_pressed			(GtkButton	*button,
					 gpointer	user_data)
{
	ProcData *procdata = user_data;
	
	toggle_infoview (procdata);
		
}	

void
cb_filter (GtkEditable *editable, gpointer data)
{
        ProcData *procdata = data;
        gchar *text, *p;
	gchar text_buff [MAX_FILTER_STRING_SIZE] = {0};
	int txt_len = 0, cnt, cnt1 = 0;
        text = gtk_editable_get_chars (editable, 0, -1);
	p = text;
        txt_len = strlen (text);
	txt_len = (txt_len > MAX_FILTER_STRING_SIZE) ? MAX_FILTER_STRING_SIZE : txt_len;
	for ( cnt = 0; cnt <txt_len; cnt ++)
	{
	   if (!isspace(*p))	
		text_buff [cnt1++] = *p;
	  p++;
	}	
	if ((strcmp (procdata->filter, text_buff) != 0) || (txt_len == 0))
	{

		if (isCompleteCycleOver == FALSE)
			while (isCompleteCycleOver == FALSE);

		if (isCompleteCycleOver == TRUE && isStopSamplingSelected == FALSE) {
			isCompleteCycleOver = FALSE;
	 		update_sensitivity_controls (FALSE);	
			proctable_clear_tree (procdata);
			strcpy (procdata->filter, text_buff);
			//while (Process_Executing) ;
			User_Selection_Changed = TRUE;
			//while (Process_Executing == TRUE);
			Writer_Func ();
			Reader_Func ();
		}
		else {
			strcpy (procdata->filter, text_buff);
			filter_all_of_same_filter_from_tree (procdata->filter, procdata);
		}
	}
        gtk_widget_grab_focus (GTK_WIDGET (editable));
        g_free (text);
}

void		
cb_search (GtkEditable *editable, gpointer data)
{
	ProcData *procdata = data;
	gchar *text;
	
	text = gtk_editable_get_chars (editable, 0, -1);
	
	proctable_search_table (procdata, text);
	gtk_widget_grab_focus (GTK_WIDGET (editable));
	g_free (text);
}


void		
cb_cpu_color_changed (GnomeColorPicker *cp, guint r, guint g, guint b,
		      guint a, gpointer data)
{
	ProcData *procdata = data;
	GConfClient *client = procdata->client;
	gchar *color;
	
	color = g_strdup_printf("#%02x%02x%02x", r, g, b);
	gconf_client_set_string (client, "/apps/gprocview/gcpu_color", color, NULL);
	g_free (color);
}


ProcInfo *selected_process = NULL;

static void
get_last_selected (GtkTreeModel *model, GtkTreePath *path, 
      		   GtkTreeIter *iter, gpointer data)
{
	ProcInfo *info;
	gtk_tree_model_get (model, iter, COL_POINTER, &info, -1);
	g_return_if_fail (info);

	selected_process = info;
}

void
cb_row_selected (GtkTreeSelection *selection, gpointer data)
{
	ProcData *procdata = data;
	
	procdata->selection = selection;
	
	/* get the most recent selected process and determine if there are
	** no selected processes 
	*/
	selected_process = NULL;	
	gtk_tree_selection_selected_foreach (procdata->selection, get_last_selected, 
					     procdata);	
	
	if (selected_process) {
		procdata->selected_process = selected_process;
	/*	
		if (procdata->config.show_more_info == TRUE)
			infoview_update (procdata);
	*/
		update_sensitivity (procdata, TRUE);
	}
	else {	
		procdata->selected_process = NULL;
		update_sensitivity (procdata, FALSE);
	}
	
}

gboolean
cb_tree_button_pressed (GtkWidget *widget, GdkEventButton *event, 
			gpointer data)
{
        ProcData *procdata = data;

/*	if (event->type == GDK_2BUTTON_PRESS) 
		toggle_infoview (procdata);
	else
*/        	do_popup_menu (procdata, event);

        return FALSE;

}

gint
cb_tree_key_press (GtkWidget *widget, GdkEventKey *event, gpointer data)
{
	ProcData *procdata = data;
		
	switch (event->keyval) {
	case GDK_Return:
	case GDK_space:
		toggle_infoview (procdata);
		break;
	default:
	}
		
	return FALSE;
		
}

void popup_position_func (GtkMenu *menu, gint *x, gint *y, gboolean *push_in,
				gpointer user_data)
{
	GtkTreeView *tree = GTK_TREE_VIEW (user_data);
	GtkWidget *widget = GTK_WIDGET (tree);
	GtkRequisition req;

	g_return_if_fail (GTK_WIDGET_REALIZED (tree));
	gdk_window_get_origin (widget->window, x, y);

	gtk_widget_size_request (GTK_WIDGET (menu), &req);

	*x += widget->allocation.width / 2;
	*y += widget->allocation.height / 3;

	*x = CLAMP (*x, 0, MAX (0, gdk_screen_width () - req.width));
	*y = CLAMP (*y, 0, MAX (0, gdk_screen_height () - req.height));
}

gint
cb_tree_popup_menu (GtkWidget *widget, gpointer data)
{
	GdkEventButton event;
	
	g_return_val_if_fail (widget !=NULL && data !=NULL, FALSE);

	event.button = 3;
	event.time = GDK_CURRENT_TIME;
	gtk_menu_popup (GTK_MENU (data), NULL, NULL,
			popup_position_func, widget,
			event.button, event.time);

	return TRUE;
}
gint
cb_timeout (gpointer data)
{
	static int i = 0;
	ProcData *procdata = data;
	int kill_ret_value = 0 ;	
	if (isSignalPending == FALSE && isStopSamplingSelected == FALSE)
	{
		isSignalPending = TRUE;
		if (isCompleteCycleOver == TRUE)
		{
			isCompleteCycleOver = FALSE;
			update_sensitivity_controls (FALSE);
			Writer_Func ();
			Reader_Func ();
		}
	}
	if (islogvisible)
	{
		if (logfilename)
			dump_log_in_file ();	
	}
	return TRUE;
}
