/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <errno.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/resource.h>
#include "procactions.h"
#include "gprocview.h"
#include "proctable.h"
#include "procdialogs.h"
#include "callbacks.h"

int nice_value;
int kill_signal;
extern gboolean isSignalPending;
extern gboolean isCompleteCycleOver;
extern gboolean isStopSamplingSelected;
int selected_cnt = 0;
static void display_system_call (gchar *cmd, int pid, ProcData *procdata);
void
destroy_cb (GtkDialog *dialog,
		gint arg1,
                gpointer user_data)
{
        gtk_widget_destroy (GTK_WIDGET(dialog));
}

static void show_ancestry_singal_process 
	(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
 	ProcData *procdata = data;
	ProcInfo *info = NULL;
	char cmdstr [1024] = {0};
	GtkWidget *main_vbox, *label;
	static GtkWidget *window;
	GtkWidget *scrolled_window;
	char temp_str[1024];
	int i, j, numlines =0;
	FILE *fileptr = NULL;

	gtk_tree_model_get (model, iter, COL_POINTER, &info, -1);
	g_return_if_fail (info);
	if (!selected_cnt) 
		selected_cnt++;
	else
			return; 

     	/* Create a new dialog window for the scrolled window to be
	* packed into.  */

	window = gtk_dialog_new_with_buttons (_("Show Ancestry"),
						NULL,
						GTK_DIALOG_DESTROY_WITH_PARENT,
						GTK_STOCK_CLOSE,
						GTK_RESPONSE_CLOSE,
						NULL);
	gtk_widget_set_size_request (window, 450, 200);				
	gtk_dialog_set_default_response (GTK_DIALOG(window),GTK_RESPONSE_CLOSE);

     	/* create a new scrolled window. */
	scrolled_window = gtk_scrolled_window_new (NULL, NULL);
	gtk_container_set_border_width (GTK_CONTAINER (scrolled_window), 10);

     	/* the policy is one of GTK_POLICY AUTOMATIC, or GTK_POLICY_ALWAYS.
	* GTK_POLICY_AUTOMATIC will automatically decide whether you need
	* scrollbars, whereas GTK_POLICY_ALWAYS will always leave the scrollbars
	* there.	The first one is the horizontal scrollbar, the second,
	* the vertical. */
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
                                     GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
	/* The dialog window is created with a vbox packed into it. */

     	gtk_box_pack_start (GTK_BOX (GTK_DIALOG(window)->vbox), scrolled_window,
			 TRUE, TRUE, 0);

     	gtk_widget_show (scrolled_window);
	sprintf (cmdstr, "/usr/bin/ptree pid %d", info->pid);
	fileptr = popen (cmdstr, "r");
	while (fgets (temp_str,1024,fileptr) != NULL)
	{
         	numlines++;
	}
        pclose (fileptr);

	main_vbox = gtk_vbox_new (FALSE, 0);
	gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (scrolled_window),
					       main_vbox);
	gtk_widget_show (main_vbox);

	 fileptr = popen (cmdstr, "r");
	/* this simply creates a grid of toggle buttons on the table
	 * to demonstrate the scrolled window. */
	for (i = 0; i < numlines; i++) {
		fgets (temp_str, 1024, fileptr);
		if( label !=NULL )	{
			label= gtk_label_new (g_strstrip(temp_str));
		}
		else {
			label= gtk_label_new ("");
		}
		gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
		gtk_box_pack_start (GTK_BOX (main_vbox), label, FALSE, FALSE, 0);
		gtk_widget_show (GTK_WIDGET(label));

		label= gtk_label_new ("");
		gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
		gtk_box_pack_start (GTK_BOX (main_vbox), label, FALSE, FALSE, 0);
		gtk_widget_show (GTK_WIDGET(label));
     	}
	pclose (fileptr);

        g_signal_connect (GTK_DIALOG (window), "response",
                        G_CALLBACK (destroy_cb), NULL);

     	gtk_widget_show (window);
}
static void
show_process_ancestry (GtkTreeModel *model,
	GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
 	ProcData *procdata = data;
	ProcInfo *info = NULL;
	gchar tempbuffer [1024] = {0};
	if (selected_cnt > 0)
		return;
	selected_cnt++;
	gtk_tree_model_get (model, iter, COL_POINTER, &info, -1);
	sprintf (tempbuffer, "/usr/X/bin/xterm -wf -e ptree pid %d", info->pid);
	system (tempbuffer);
}

void show_ancestry_dialog (ProcData *procdata) 
{
 	if (!procdata->selection)
		return;
	selected_cnt = 0;
	gtk_tree_selection_selected_foreach (procdata->selection, 
		show_ancestry_singal_process, procdata);
	
}

static void show_trace_sys_call(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
	ProcInfo *info = NULL;
	gchar temp [100];

	if (selected_cnt == 0)
                selected_cnt++;
        else
                return;

	gtk_tree_model_get (model, iter, COL_POINTER, &info, -1);
	g_return_if_fail (info);
        sprintf (temp, "/usr/X/bin/xterm +wf -e truss -t all -p %d", info->pid);
        system (temp);
}

static void display_system_call (gchar *cmd, int pid, ProcData *procdata)
{
        ProcInfo *info = NULL;
        gint error;
        gchar *error_msg;
        GtkWidget *dialog;
        char cmdstr [1024] = {0};
        GtkWidget *messagebox1;
        GtkWidget *main_vbox, *label;
        gchar *text = _("Show Stack\n");
        static GtkWidget *window;
        GtkWidget *scrolled_window, *event_box;
        GtkWidget *button, *table;
        char buffer[32], temp_str[1024];
        int i, j, numlines =0;
        FILE *fileptr = NULL;
        gint resp_id;
        GtkJustification just;
        //gtk_tree_model_get (model, iter, COL_POINTER, &info, -1);
        g_return_if_fail (info);

        /* Create a new dialog window for the scrolled window to be
        * packed into.  */
        window = gtk_dialog_new ();
        gtk_window_set_title (GTK_WINDOW (window), "Trace System Call");
        gtk_container_set_border_width (GTK_CONTAINER (window), 0);
        gtk_widget_set_usize(window, 600, 300);

        /* create a new scrolled window. */
        scrolled_window = gtk_scrolled_window_new (NULL, NULL);
        gtk_container_set_border_width (GTK_CONTAINER (scrolled_window), 10);

        /* the policy is one of GTK_POLICY AUTOMATIC, or GTK_POLICY_ALWAYS.
        * GTK_POLICY_AUTOMATIC will automatically decide whether you need
        * scrollbars, whereas GTK_POLICY_ALWAYS will always leave the scrollbars
        * there.  The first one is the horizontal scrollbar, the second,
        * the vertical. */
        gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
                                     GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
        /* The dialog window is created with a vbox packed into it. */

        gtk_box_pack_start (GTK_BOX (GTK_DIALOG(window)->vbox), scrolled_window,
                         TRUE, TRUE, 0);

        gtk_widget_show (scrolled_window);
        sprintf (cmdstr, "/usr/bin/truss -t all", pid);
        fileptr = popen (cmdstr, "r");
        while (fgets (temp_str,1024,fileptr) != NULL)
        {
                numlines++;
        }
        pclose (fileptr);

        table = gtk_table_new (numlines, 1, TRUE);
        gtk_table_set_row_spacings (GTK_TABLE(table), 0);
        gtk_table_set_row_spacings (GTK_TABLE (table), 10);
        gtk_table_set_col_spacings (GTK_TABLE (table), 10);

        /* pack the table into the scrolled window */
        gtk_scrolled_window_add_with_viewport (
                    GTK_SCROLLED_WINDOW (scrolled_window), table);
        gtk_widget_show (table);

        fileptr = popen (cmdstr, "r");
        /* this simply creates a grid of toggle buttons on the table
        * to demonstrate the scrolled window. */
        for (i = 0; i < numlines; i++) {
                fgets (temp_str, 1024, fileptr);
                label= gtk_label_new (temp_str);
                gtk_label_set_justify( GTK_LABEL(label), GTK_JUSTIFY_LEFT);
             gtk_table_attach_defaults (GTK_TABLE (table), GTK_WIDGET(label),
                                      0, 1, i+1, i+2);
                gtk_widget_show (GTK_WIDGET(label));
        }
        pclose (fileptr);

        if (numlines == 0)
        {
                label= gtk_label_new ("permission denied");
                gtk_label_set_justify( GTK_LABEL(label),
                             GTK_JUSTIFY_LEFT);

                gtk_table_attach_defaults (GTK_TABLE (table),
                        GTK_WIDGET(label), 0, 1, 0, 1);
                gtk_widget_show (GTK_WIDGET(label));
        }
        /* Add a "close" button to the bottom of the dialog */
        //button = gtk_button_new_with_label ("close");
        button = gtk_dialog_add_button (GTK_DIALOG(window), "Close",-7);
        g_signal_connect (GTK_DIALOG (window), "response",
                        G_CALLBACK (destroy_cb), NULL);


        /* this makes it so the button is the default. */

        GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
        gtk_box_pack_start (GTK_BOX (GTK_DIALOG (window)->action_area), button, TRUE, TRUE, 0);

        /* This grabs this button to be the default button. Simply hitting
        * the "Enter" key will cause this button to activate. */
        gtk_widget_grab_default (button);
        gtk_widget_show (button);
        gtk_widget_show (window);
}	

void trace_system_call (ProcData *procdata)
{
	if (!procdata->selection)
		return;
	selected_cnt = 0;
	gtk_tree_selection_selected_foreach 
		(procdata->selection, show_trace_sys_call,procdata);
}
static void
show_stack_single_process (GtkTreeModel *model, 
	GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
 	ProcData *procdata = data;
	ProcInfo *info = NULL;
	char cmdstr [1024] = {0};
	GtkWidget *main_vbox, *label;
	static GtkWidget *window;
	GtkWidget *scrolled_window;
	char temp_str[1024];
	int i, j, numlines =0;
	FILE *fileptr = NULL;

	gtk_tree_model_get (model, iter, COL_POINTER, &info, -1);
	g_return_if_fail (info);
   	
	if (selected_cnt)
		return;
	selected_cnt++; 

     	/* Create a new dialog window for the scrolled window to be
      	* packed into.  */
	
	window = gtk_dialog_new_with_buttons (_("Show Stack"),
						NULL,
						GTK_DIALOG_DESTROY_WITH_PARENT,	
						GTK_STOCK_CLOSE,
						GTK_RESPONSE_CLOSE,
						NULL);
	gtk_widget_set_size_request (window, 450, 200);
	gtk_dialog_set_default_response (GTK_DIALOG(window),GTK_RESPONSE_CLOSE);


     	/* create a new scrolled window. */
     	scrolled_window = gtk_scrolled_window_new (NULL, NULL);
        gtk_container_set_border_width (GTK_CONTAINER (scrolled_window), 10);

     	/* the policy is one of GTK_POLICY AUTOMATIC, or GTK_POLICY_ALWAYS.
      	* GTK_POLICY_AUTOMATIC will automatically decide whether you need
      	* scrollbars, whereas GTK_POLICY_ALWAYS will always leave the scrollbars
      	* there.  The first one is the horizontal scrollbar, the second,
      	* the vertical. */
     	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
                                     GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
     	/* The dialog window is created with a vbox packed into it. */                                   
     	gtk_box_pack_start (GTK_BOX (GTK_DIALOG(window)->vbox), scrolled_window,
                         TRUE, TRUE, 0);

     	gtk_widget_show (scrolled_window);
	sprintf (cmdstr, "/usr/bin/pstack pid %d", info->pid);
        fileptr = popen (cmdstr, "r");
        while (fgets (temp_str,1024,fileptr) != NULL)
        {      
                numlines++;
        }
        pclose (fileptr);

	main_vbox = gtk_vbox_new (FALSE, 0);
	gtk_scrolled_window_add_with_viewport ( GTK_SCROLLED_WINDOW (scrolled_window), 
						main_vbox);
	gtk_widget_show (main_vbox);

       	fileptr = popen (cmdstr, "r");
     	/* this simply creates a grid of toggle buttons on the table
      	* to demonstrate the scrolled window. */
     	for (i = 0; i < numlines; i++) {
        	fgets (temp_str, 1024, fileptr);
		if( label !=NULL )	{
			label= gtk_label_new (g_strstrip(temp_str));
		}
		else {
			label= gtk_label_new ("");
		}
		gtk_misc_set_alignment (GTK_MISC (label), 0, 0);	
		gtk_box_pack_start (GTK_BOX (main_vbox), label, FALSE, FALSE, 0);
		gtk_widget_show (GTK_WIDGET(label));

		label= gtk_label_new ("");
		gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
		gtk_box_pack_start (GTK_BOX (main_vbox), label, FALSE, FALSE, 0);

		gtk_widget_show (GTK_WIDGET(label));
        }
    	pclose (fileptr);

	if (numlines == 0)
	{
		label= gtk_label_new ("permission denied");
		gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
		gtk_box_pack_start (GTK_BOX (main_vbox), label, FALSE, FALSE, 0);

	   	gtk_widget_show (GTK_WIDGET(label));	
	} 
	
	g_signal_connect (GTK_DIALOG (window), "response",
                        G_CALLBACK (destroy_cb), NULL);
     	gtk_widget_show (window);
}

static void
cnt_selected (GtkTreeModel *model,
	GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
 	ProcData *procdata = data;
	ProcInfo *info = NULL;
	gchar tempbuffer [1024] = {0};
	if (selected_cnt > 0)	
		return;
	selected_cnt++;
	gtk_tree_model_get (model, iter, COL_POINTER, &info, -1);
	sprintf (tempbuffer, "/usr/X/bin/xterm +wf -e pstack pid %d", info->pid);
	system (tempbuffer);
}
void show_stack_dialog (ProcData *procdata)
{
        if (!procdata->selection)
                return;
	selected_cnt = 0;
	gtk_tree_selection_selected_foreach (procdata->selection , show_stack_single_process, procdata);
	//gtk_tree_selection_selected_foreach (procdata->selection , cnt_selected, procdata);

}
static void
show_trace_children (GtkTreeModel *model, 
	GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
 	ProcData *procdata = data;
	ProcInfo *info = NULL;
	gint error;
	gchar *error_msg;
	GtkWidget *dialog;
	char cmdstr [1024] = {0};
	GtkWidget *messagebox1;
	GtkWidget *main_vbox, *label;
	gchar *text = _("Trace Children\n");
	static GtkWidget *window;
	GtkWidget *scrolled_window, *event_box;
	GtkWidget *button, *table;
	char buffer[32], temp_str[1024];
	int i, j, numlines =0;
	FILE *fileptr = NULL;
	GtkJustification just;
	gtk_tree_model_get (model, iter, COL_POINTER, &info, -1);
	g_return_if_fail (info);
	if (selected_cnt > 0)
		return;
	selected_cnt++;
  	sprintf (cmdstr, "/usr/X/bin/xterm +wf -e truss -fp %d ", info->pid);	
	system (cmdstr);	
/*
     	window = gtk_dialog_new ();
     	gtk_window_set_title (GTK_WINDOW (window), "Trace Children");
     	gtk_container_set_border_width (GTK_CONTAINER (window), 0);
     	gtk_widget_set_usize(window, 600, 300);

     	scrolled_window = gtk_scrolled_window_new (NULL, NULL);
	gtk_container_set_border_width (GTK_CONTAINER (scrolled_window), 10);
     	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
                                     GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);

     	gtk_box_pack_start (GTK_BOX (GTK_DIALOG(window)->vbox), scrolled_window,
                         TRUE, TRUE, 0);

     	gtk_widget_show (scrolled_window);
	sprintf (cmdstr, "/usr/bin/truss -fp %d", info->pid);
        fileptr = popen (cmdstr, "r");
	while (fgets (temp_str,1024,fileptr) != NULL)
	{
		numlines++;
	}
	pclose (fileptr);

     	table = gtk_table_new (numlines, 1, TRUE);
	gtk_table_set_row_spacings (GTK_TABLE(table), 0);
     	gtk_table_set_row_spacings (GTK_TABLE (table), 10);
     	gtk_table_set_col_spacings (GTK_TABLE (table), 10);

     	gtk_scrolled_window_add_with_viewport (
                    GTK_SCROLLED_WINDOW (scrolled_window), table);
     	gtk_widget_show (table);

	fileptr = popen (cmdstr, "r");
     	for (i = 0; i < numlines; i++) {
		fgets (temp_str, 1024, fileptr);
		label= gtk_label_new (temp_str);
		gtk_label_set_justify( GTK_LABEL(label),
                             GTK_JUSTIFY_LEFT);

          	gtk_table_attach_defaults (GTK_TABLE (table), GTK_WIDGET(label),
                                      0, 1, i+1, i+2);
           	gtk_widget_show (GTK_WIDGET(label));
	}
    	pclose (fileptr);
        
	if (numlines == 0)
        {
                label= gtk_label_new ("permission denied");
                gtk_label_set_justify( GTK_LABEL(label),
                             GTK_JUSTIFY_LEFT);

                gtk_table_attach_defaults (GTK_TABLE (table),
                        GTK_WIDGET(label), 0, 1, 0, 1);
                gtk_widget_show (GTK_WIDGET(label));
        }

     	button = gtk_button_new_with_label ("close");
	g_signal_connect_swapped (G_OBJECT (button), "response",
       			G_CALLBACK (gtk_widget_destroy), window);


     	GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
     	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (window)->action_area), button, TRUE, TRUE, 0);

     	gtk_widget_grab_default (button);
     	gtk_widget_show (button);

     	gtk_widget_show (window);
*/
}

void trace_children_dialog (ProcData *procdata)
{
 	if (!procdata->selection)
		return;
	selected_cnt = 0;	
	gtk_tree_selection_selected_foreach (procdata->selection, show_trace_children,
                                             procdata);

}


static void
debug_each_process (GtkTreeModel *model, 
	GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{	
 	ProcData *procdata = data;
	ProcInfo *info = NULL;
	char debug_cmd [MAX_TEMP_BUFFER_SIZE];
	gchar *workshop = NULL;

	gtk_tree_model_get (model, iter, COL_POINTER, &info, -1);
	g_return_if_fail (info);	
	if (selected_cnt)
		return;
	selected_cnt++;

	workshop = g_find_program_in_path ("workshop");

	if (workshop == NULL)
	{
		gchar *error = "Debugger not found";
                GtkWidget *dialog = gtk_message_dialog_new (NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
                                                 GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
                                                 "%s", error, NULL);
                gtk_dialog_run (GTK_DIALOG (dialog));
                gtk_widget_destroy (dialog);
                return;
	}

	sprintf (debug_cmd, "%s %d ", workshop, info->pid);
	system (debug_cmd);	
	g_free (workshop);
}

void debug_process (ProcData *procdata)
{
 	if (!procdata->selection)
		return;
	selected_cnt = 0;
	gtk_tree_selection_selected_foreach (procdata->selection, debug_each_process,
                                             procdata);
}
static void 
send_signle_to_process  (GtkTreeModel *model, 
	GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
        ProcInfo *info = NULL;
        gint error;
 	GtkWidget *dialog;
  	GtkWidget *hbox;
  	GtkWidget *stock;
  	GtkWidget *label;
	GtkWidget *window = NULL;
	GList *glist = NULL;
	GtkWidget *combo;

  	gint response;
	if (!selected_cnt)
		selected_cnt++;
	else
		return;

        gtk_tree_model_get (model, iter, COL_POINTER, &info, -1);
        g_return_if_fail (info);
  	dialog = gtk_dialog_new_with_buttons ("Send Signal",
                                        GTK_WINDOW (window),
                                        GTK_DIALOG_MODAL| GTK_DIALOG_DESTROY_WITH_PARENT,
                                        GTK_STOCK_CANCEL,
                                        GTK_RESPONSE_CANCEL,
                                        GTK_STOCK_OK,
                                        GTK_RESPONSE_OK,
                                        NULL);

  	hbox = gtk_hbox_new (FALSE, 8);
  	gtk_container_set_border_width (GTK_CONTAINER (hbox), 8);
  	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), hbox, FALSE, FALSE, 0);

  	stock = gtk_image_new_from_stock (GTK_STOCK_DIALOG_INFO, GTK_ICON_SIZE_DIALOG);
  	gtk_box_pack_start (GTK_BOX (hbox), stock, FALSE, FALSE, 0);

	label = gtk_label_new_with_mnemonic (("Choose Signal"));
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);

	combo = gtk_combo_new();
	gtk_box_pack_start (GTK_BOX (hbox), combo, TRUE, TRUE, 0);

	glist = g_list_append(glist, "INT");
	glist = g_list_append(glist, "HUP");
	glist = g_list_append(glist, "ALRM");
	glist = g_list_append(glist, "ABRT");
	glist = g_list_append(glist, "TSTP");	
	glist = g_list_append(glist, "CONT");
	glist = g_list_append(glist, "USR1");
	gtk_combo_set_popdown_strings( GTK_COMBO(combo), glist) ;

  	gtk_widget_show_all (hbox);
  	response = gtk_dialog_run (GTK_DIALOG (dialog));

  	if (response == GTK_RESPONSE_OK)
    	{
		gchar *sel_sig;	
		int kill_ret_val = 0;
		gchar *input_text = g_strdup (
			gtk_entry_get_text (GTK_ENTRY(GTK_COMBO(combo)->entry)));
		if (!strlen(input_text)) 
		{
			GtkWidget* err_dialog;
			gchar* error = g_strdup("Wrong signal");
                        err_dialog = gtk_message_dialog_new (NULL,
                                GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_ERROR,
                                GTK_BUTTONS_OK, "%s", error, NULL);

                        g_free (error);
                        gtk_widget_destroy (err_dialog);
                        gtk_widget_destroy (dialog);
			return;
		}	
 		sel_sig = g_ascii_strup (input_text, strlen (input_text));
		if (g_ascii_strcasecmp (sel_sig, "KILL") == 0) 
		{
			GtkWidget* err_dialog;
			kill_ret_val = kill (info->pid , SIGKILL);
			if (kill_ret_val > 0)
			{
			gchar *error = g_strdup_printf (_("%s Wrong signal."), sel_sig);
			dialog = gtk_message_dialog_new (NULL, 
				GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_ERROR, 
				GTK_BUTTONS_OK, "%s", error, NULL);
			gtk_dialog_run (GTK_DIALOG (err_dialog));
			gtk_widget_destroy (err_dialog);
			g_free (error);	
			}		
		}
       		else if (g_ascii_strcasecmp (sel_sig, "ABRT") == 0)
		{
			GtkWidget* err_dialog;
                		kill_ret_val = kill (info->pid , SIGABRT);
			if (kill_ret_val > 0)
			{
                        gchar *error = g_strdup_printf (_("%s Wrong signal."), sel_sig);
			dialog = gtk_message_dialog_new (NULL, 
                                GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_ERROR, 
                                GTK_BUTTONS_OK, "%s", error, NULL);

			gtk_dialog_run (GTK_DIALOG (err_dialog));
                        gtk_widget_destroy (err_dialog);   
                        g_free (error);
			}

		}
                else if (g_ascii_strcasecmp (sel_sig, "ALRM") == 0)
		{
			GtkWidget* err_dialog;
                	kill_ret_val = kill (info->pid , SIGALRM);
			if (kill_ret_val > 0)
			{
                        gchar *error = g_strdup_printf (_("%s Wrong signal."), sel_sig);
			dialog = gtk_message_dialog_new (NULL, 
                                GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_ERROR, 
                                GTK_BUTTONS_OK, "%s", error, NULL);

			gtk_dialog_run (GTK_DIALOG (err_dialog));
                        gtk_widget_destroy (err_dialog);   
                        g_free (error);
			}

		}
                else if (g_ascii_strcasecmp (sel_sig, "HUP") == 0)
                {
                        GtkWidget* err_dialog;
                        kill_ret_val = kill (info->pid , SIGHUP);
                        if (kill_ret_val > 0)
                        {
                        gchar *error = g_strdup_printf (_("%s Wrong signal."), sel_sig);
                        dialog = gtk_message_dialog_new (NULL,
                                GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_ERROR,
                                GTK_BUTTONS_OK, "%s", error, NULL);

                        gtk_dialog_run (GTK_DIALOG (err_dialog));
                        gtk_widget_destroy (err_dialog);
                        g_free (error);
                        }
                }
                else if (g_ascii_strcasecmp (sel_sig, "TSTP") == 0)
                {
                        GtkWidget* err_dialog;
                        kill_ret_val = kill (info->pid , SIGTSTP);
                        if (kill_ret_val > 0)
                        {
                        gchar *error = g_strdup_printf (_("%s Wrong signal."), sel_sig);
                        dialog = gtk_message_dialog_new (NULL,
                                GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_ERROR,
                                GTK_BUTTONS_OK, "%s", error, NULL);

                        gtk_dialog_run (GTK_DIALOG (err_dialog));
                        gtk_widget_destroy (err_dialog);
                        g_free (error);
                        }

                }
                else if (g_ascii_strcasecmp (sel_sig, "CONT") == 0)
                {
                        GtkWidget* err_dialog;
                        kill_ret_val = kill (info->pid , SIGCONT);
                        if (kill_ret_val > 0)
                        {
                        gchar *error = g_strdup_printf (_("%s Wrong signal."), sel_sig);
                        dialog = gtk_message_dialog_new (NULL,
                                GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_ERROR,
                                GTK_BUTTONS_OK, "%s", error, NULL);

                        gtk_dialog_run (GTK_DIALOG (err_dialog));
                        gtk_widget_destroy (err_dialog);
                        g_free (error);
                        }

                }
                else if (g_ascii_strcasecmp (sel_sig, "USR1") == 0)
                {
                        GtkWidget* err_dialog;
                        kill_ret_val = kill (info->pid , SIGUSR1);
                        if (kill_ret_val > 0)
                        {
                        gchar *error = g_strdup_printf (_("%s Wrong signal."), sel_sig);
                        dialog = gtk_message_dialog_new (NULL,
                                GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_ERROR,
                                GTK_BUTTONS_OK, "%s", error, NULL);

                        gtk_dialog_run (GTK_DIALOG (err_dialog));
                        gtk_widget_destroy (err_dialog);
                        g_free (error);
                        }

                }
                else if (g_ascii_strcasecmp (sel_sig, "INT") == 0)
                {
                        GtkWidget* err_dialog;
                        kill_ret_val = kill (info->pid , SIGINT);
                        if (kill_ret_val > 0)
                        {
                        gchar *error = g_strdup_printf (_("%s Wrong signal."), sel_sig);
                        dialog = gtk_message_dialog_new (NULL,
                                GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_ERROR,
                                GTK_BUTTONS_OK, "%s", error, NULL);

                        gtk_dialog_run (GTK_DIALOG (err_dialog));
                        gtk_widget_destroy (err_dialog);
                        g_free (error);
                        }

                }
		else
		{	
			GtkWidget* err_dialog;
                        gchar *error = g_strdup_printf (_("%s Wrong signal."), input_text);	
			err_dialog = gtk_message_dialog_new (NULL,
				GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_ERROR,
				GTK_BUTTONS_OK, "%s", error, NULL);

			gtk_dialog_run (GTK_DIALOG (err_dialog));
			gtk_widget_destroy (err_dialog);
			g_free (error);
		}
		g_free (input_text); g_free (sel_sig);

    	}

  	gtk_widget_destroy (dialog);
}

void send_signal_dialog (ProcData *procdata)
{
        if (!procdata->selection)
                return;
	selected_cnt = 0;
        gtk_tree_selection_selected_foreach (procdata->selection, 
		send_signle_to_process,  procdata);	
	//TODO : After sending signal update view.
 	//proctable_clear_tree (procdata);

	if (isSignalPending == FALSE && isStopSamplingSelected == FALSE)
	{
		isSignalPending = TRUE;
		if (isCompleteCycleOver == FALSE)
			while (isCompleteCycleOver == FALSE);
		if (isCompleteCycleOver == TRUE)
		{
			update_sensitivity_controls (FALSE);
			isCompleteCycleOver = FALSE;
			Writer_Func ();
			Reader_Func ();
		}
	}

}


static void
kill_single_process (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
	ProcData *procdata = data;
	ProcInfo *info;
	int error;
	GtkWidget *dialog;
        gchar *error_msg;
	gchar *error_critical;

	gtk_tree_model_get (model, iter, COL_POINTER, &info, -1);
	g_return_if_fail (info);
		
	/* Author:  Tige Chastian
	   Date:  8/18/01 
	   Added dialogs for errors on kill.  
	   Added sigterm fail over to sigkill 
	*/

        if (info->pid == 0) {
           	error_msg = g_strdup_printf (_("Process Name : %s \n\nOperation not allowed\n."), info->name); 
                dialog = gtk_message_dialog_new (NULL, 
			 GTK_DIALOG_DESTROY_WITH_PARENT, 
			 GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, "%s", error_msg, 
			 NULL);
                gtk_dialog_run (GTK_DIALOG (dialog));
                gtk_widget_destroy (dialog);
                g_free (error_msg);
                return;
        }

        error = kill (info->pid, kill_signal);
	if (error == -1)
	{
		switch (errno) {
			case ESRCH:
				break;
			case EPERM:
	
           			error_msg = g_strdup_printf (_("Process Name : %s \n\nYou do not have permission to end this process."), info->name); 
                dialog = gtk_message_dialog_new (NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
                                                 GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
                                                 "%s", error_msg, NULL);
                gtk_dialog_run (GTK_DIALOG (dialog));
                gtk_widget_destroy (dialog);
                g_free (error_msg);
	/*
				error_msg = g_strdup_printf (_("Process Name : %s \n\nYou do not have permission to end this process. You can enter the root password to gain the necessary permission."), info->name);
				procdialog_create_root_password_dialog (0, procdata, 
									info->pid, kill_signal,
									error_msg);
				g_free (error_msg);
*/
				break;	
			default: 
				error = kill (info->pid, SIGKILL);
				if (error == -1)
				{
					switch (errno) {
					case ESRCH:
						break;
					default:
						dialog = gtk_message_dialog_new (NULL,
							       GTK_DIALOG_DESTROY_WITH_PARENT,
                                  			       GTK_MESSAGE_ERROR,
                                  			       GTK_BUTTONS_OK,
                                  			       "%s",
                                  			      _("An error occured while killing the process."),
                                  			      NULL); 
						gtk_dialog_run (GTK_DIALOG (dialog));
						gtk_widget_destroy (dialog);
					}
				}
			
                	}
	}
}

void
kill_process (ProcData *procdata, int sig)
{
	/* EEEK - ugly hack - make sure the table is not updated as a crash
	** occurs if you first kill a process and the tree node is removed while
	** still in the foreach function
	*/
	gtk_timeout_remove (procdata->timeout);	
	
	kill_signal = sig;	
	
	gtk_tree_selection_selected_foreach (procdata->selection, kill_single_process, 
					     procdata);
	
	if (isSignalPending == FALSE && isStopSamplingSelected == FALSE)
	{
		isSignalPending = TRUE;
		if (isCompleteCycleOver == FALSE)
			while (isCompleteCycleOver == FALSE);
		if (isCompleteCycleOver == TRUE)
		{
			update_sensitivity_controls (FALSE);
			isCompleteCycleOver = FALSE;
                        //proctable_clear_tree (procdata);
			Writer_Func ();
			Reader_Func ();
		}
	}
	procdata->timeout = gtk_timeout_add (procdata->config.update_interval,
					     cb_timeout, procdata);	    
	proctable_update_all (procdata);
	
}
