/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */
#ifndef _PROCACTIONS_H_
#define _PROCACTIONS_H_

#include "gprocview.h"
 
//void		renice (ProcData *procdata, int pid, int nice);
void		kill_process (ProcData *procdata, int sig);

void show_stack_dialog (ProcData *used_data);
void show_ancestry_dialog (ProcData *procdata);
#endif

