/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "favorites.h"
#include "proctable.h"

GtkWidget *blacklist_dialog = NULL;
GtkWidget *proctree = NULL;
gint initial_blacklist_num = 0; /* defined in order to prune off entries from config file */

void
add_to_blacklist (ProcData *procdata, gchar *name)
{
	gchar *process = g_strdup (name);
	procdata->blacklist = g_list_append (procdata->blacklist, process);
	procdata->blacklist_num++;
	
	if (blacklist_dialog) {
		GtkTreeModel *model;
		GtkTreeIter row;
		
		model = gtk_tree_view_get_model (GTK_TREE_VIEW (proctree));
		gtk_tree_store_insert (GTK_TREE_STORE (model), &row, NULL, 0);
		gtk_tree_store_set (GTK_TREE_STORE (model), &row, 0, name, -1);
	}
		
}

GList *removed_processes = NULL;

static void
add_single_to_blacklist (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
	ProcData *procdata = data;
	ProcInfo *info = NULL;
	
	gtk_tree_model_get (model, iter, COL_POINTER, &info, -1);
	g_return_if_fail (info);
	
	add_to_blacklist (procdata, info->name);
	
	if (info->visible) 
		removed_processes = g_list_append (removed_processes, info);
}

static void
remove_all_of_same_name_from_tree (ProcInfo *info, ProcData *procdata)
{
	GList *list = procdata->info;
	
	while (list) {
		ProcInfo *tmp = list->data;
		
		if (g_strcasecmp (info->name, tmp->name) == 0) 
			remove_info_from_tree (tmp, procdata);
			
		list = g_list_next (list);
	}

}

void add_all_blacklisted_processes (ProcData *procdata)
{
 	GList *black_list = procdata->blacklist;
	GList *info_list = procdata->info; 
	gchar *name,*name1;	
        while (black_list) {
		name = black_list->data;
		info_list = procdata->info;	
		while (info_list) {
			ProcInfo *temp1 = info_list->data;	
                	if (!g_strcasecmp (temp1->name, name)) {
                        	procdata->blacklist = g_list_remove (procdata->blacklist, info_list->data); 
				procdata->blacklist_num --;
				insert_info_to_tree (temp1, procdata);
			}
			info_list = g_list_next (info_list);
		}
		black_list = g_list_next (black_list);
	}
}

void
add_selected_to_blacklist (ProcData *procdata)
{
	if (!procdata->selection)
		return;
		
	gtk_tree_selection_selected_foreach (procdata->selection, 
					     add_single_to_blacklist, procdata);
					     
	while (removed_processes) {
		ProcInfo *info = removed_processes->data;
		remove_all_of_same_name_from_tree (info, procdata);
		
		removed_processes = g_list_next (removed_processes);
	}
}

void
remove_from_blacklist (ProcData *procdata, gchar *name)
{
	GList *list = procdata->blacklist;
	
	while (list) {
		if (!g_strcasecmp (list->data, name)) {
			procdata->blacklist = g_list_remove (procdata->blacklist, list->data);
			procdata->blacklist_num --;
			return;
		}
		
		list = g_list_next (list);
	}
	
}

gboolean
is_process_blacklisted (ProcData *procdata, gchar *name)
{
	GList *list = procdata->blacklist;
	
	if (!list)
	{
		return FALSE;
	}
	
	while (list)
	{
		gchar *process = list->data;
		if (!g_strcasecmp (process, name))
			return TRUE;
		
		list = g_list_next (list);
	}
	
	return FALSE;

}

void 
save_blacklist (ProcData *procdata, GConfClient *client)
{

	GList *list = procdata->blacklist;
	gint i = 0;
	
	while (list)
	{
		gchar *name = list->data;
		gchar *config = g_strdup_printf ("%s%d", "/apps/gprocview/process", i);
		gconf_client_set_string (client, config, name, NULL);
		g_free (config); 
		i++;
		
		list = g_list_next (list);
	}
	
	for (i = initial_blacklist_num; i >= procdata->blacklist_num; i--)
	{
		gchar *config = g_strdup_printf ("%s%d", "/apps/gprocview/process", i);
		gconf_client_unset (client, config, NULL);
		g_free (config);
	} 

}

void get_blacklist (ProcData *procdata, GConfClient *client)
{
	gint i = 0;
	gboolean done = FALSE;
	
	while (!done)
	{
		gchar *config = g_strdup_printf ("%s%d", "/apps/gprocview/process", i);
		gchar *process;
		
		process = gconf_client_get_string (client, config, NULL);
		g_free (config);
		if (process)
		{
			add_to_blacklist (procdata, process);
			g_free (process);
		}
		else
			done = TRUE;
		i++;
	}
	
	procdata->blacklist_num = i - 1;
	initial_blacklist_num = i - 1;

}

static void 
fill_tree_with_info (ProcData *procdata, GtkWidget *tree)
{
	GList *blacklist = procdata->blacklist;
	GtkTreeModel *model;
	GtkTreeIter row;
	
	model = gtk_tree_view_get_model (GTK_TREE_VIEW (tree));
	
	/* add the blacklist */
	while (blacklist)
	{
		gtk_tree_store_insert (GTK_TREE_STORE (model), &row, NULL, 0);
		gtk_tree_store_set (GTK_TREE_STORE (model), &row, 0, blacklist->data, -1);
		blacklist = g_list_next (blacklist);
	}
}


static GtkWidget *
create_tree (ProcData *procdata)
{
	GtkWidget *scrolled;
	GtkWidget *tree;
	GtkTreeStore *model;
	GtkTreeViewColumn *column;
	GtkTreeSelection *selection;
  	GtkCellRenderer *cell_renderer;
		
	scrolled = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled),
                                  	GTK_POLICY_AUTOMATIC,
                                  	GTK_POLICY_AUTOMATIC);
	
	model = gtk_tree_store_new (1, G_TYPE_STRING);
	
	tree = gtk_tree_view_new_with_model (GTK_TREE_MODEL (model));
	g_object_unref (G_OBJECT (model));
  	
  	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree));
  	gtk_tree_selection_set_mode (selection, GTK_SELECTION_MULTIPLE);
  	
  	cell_renderer = gtk_cell_renderer_text_new ();
  	column = gtk_tree_view_column_new_with_attributes ("hello",
						    	   cell_renderer,
						     	   "text", 0,
						     	   NULL);
	gtk_tree_view_column_set_sort_column_id (column, 0);
					     	   
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);
	
	gtk_container_add (GTK_CONTAINER (scrolled), tree);
	
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
					      0,
					      GTK_SORT_ASCENDING);
	
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (tree), FALSE);
	  	
	fill_tree_with_info (procdata, tree);
	gtk_tree_selection_select_all(selection);

	proctree = tree;
	
	return scrolled;

}

GList *removed_iters = NULL;

static void
insert_all_of_same_name_from_tree (gchar *name, ProcData *procdata)
{
	GList *list = procdata->info;
	
	while (list) {
		ProcInfo *tmp = list->data;
		
		if (g_strcasecmp (name, tmp->name) == 0) 
			insert_info_to_tree (tmp, procdata);
			
		list = g_list_next (list);
	}

}

static void
remove_item (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
	ProcData *procdata = data;
	GtkTreeIter *iter_copy;
	gchar *process = NULL;
	
	gtk_tree_model_get (model, iter, 0, &process, -1);
	
	if (process) {
		remove_from_blacklist (procdata, process);
		insert_all_of_same_name_from_tree (process, procdata);			
	}
		
	iter_copy = gtk_tree_iter_copy (iter);	
	removed_iters = g_list_append (removed_iters, iter_copy);
}

static void
remove_button_clicked (gpointer data)
{
	ProcData *procdata = data;
	GtkTreeModel *model;
	GtkTreeSelection *selection = NULL;
	
	model = gtk_tree_view_get_model (GTK_TREE_VIEW (proctree));
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (proctree));
	
	gtk_tree_selection_selected_foreach (selection, remove_item, procdata); 
	
	while (removed_iters) {
		GtkTreeIter *iter = removed_iters->data;
		
		gtk_tree_store_remove (GTK_TREE_STORE (model), iter);
		gtk_tree_iter_free (iter);
		
		removed_iters = g_list_next (removed_iters);
	}
	
}

static void
response_blacklist_dialog (GtkDialog *dialog, gint id, gpointer data)
{
	if(id == 1) {
		remove_button_clicked(data);
		update_hide_process_menu(data);
		if ( procdata->blacklist_num)
			return;
	}
	gtk_widget_destroy (blacklist_dialog);
	blacklist_dialog = NULL;
}

void create_blacklist_dialog (ProcData *procdata)
{
	GtkWidget *main_vbox;
	GtkWidget *scrolled;
	GtkWidget *label;
	GtkWidget *dialog;

	update_hide_process_menu(procdata);
	if (blacklist_dialog) {
		gdk_window_raise (blacklist_dialog->window);
		return;
	}

	blacklist_dialog = gtk_dialog_new_with_buttons (_("Show Hidden Processes"),
							  NULL,
							  GTK_DIALOG_DESTROY_WITH_PARENT,
							  NULL);
	gtk_window_set_policy (GTK_WINDOW (blacklist_dialog), FALSE, TRUE, FALSE);	
	gtk_window_set_default_size (GTK_WINDOW (blacklist_dialog), 320, 275);
	gtk_dialog_add_buttons(GTK_DIALOG(blacklist_dialog),"SHOW",1,GTK_STOCK_CLOSE,
			       GTK_RESPONSE_CLOSE,NULL);
	
	main_vbox = GTK_DIALOG (blacklist_dialog)->vbox;
	label = gtk_label_new (_("Hidden  Processes"));
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_box_pack_start (GTK_BOX (main_vbox), label, FALSE, FALSE, 0);
	scrolled = create_tree (procdata);
	gtk_box_pack_start (GTK_BOX (main_vbox), scrolled, TRUE, TRUE, 0);

	g_signal_connect (G_OBJECT (blacklist_dialog), "response",
			  G_CALLBACK (response_blacklist_dialog), procdata);
	gtk_widget_show_all (blacklist_dialog);
}

void
filter_all_of_same_filter_from_tree (char *filter, ProcData *procdata)
{
	GList *list = procdata->info;

	while (list) {
		ProcInfo *tmp = list->data;
		char *tmp_str = g_strdup_printf ("%d %s %s %f %d %d %d %s %s",tmp->pid,tmp->name,tmp->user,tmp->cpu,tmp->mem,tmp->vmsize,tmp->parent_pid,tmp->arguments,tmp->status);

		if (g_strrstr (tmp_str, filter) != NULL) {
			if (!tmp->visible)
				insert_info_to_tree (tmp, procdata);
		}
		else if (tmp->visible)
			remove_info_from_tree (tmp, procdata);

		list = g_list_next (list);
	}
}
