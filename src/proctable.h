/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef _PROCTABLE_H_
#define _PROCTABLE_H_

/*
enum
{
	COL_NAME = 0,
	COL_ARGS,
	COL_USER,
	COL_STARTED,
	COL_MEM,
	COL_VMSIZE,
	//COL_MEMRES,
	//COL_MEMSHARED,
	//COL_MEMRSS,
	COL_CPU,
	COL_PPID,
	COL_PID,
	COL_PIXBUF,
	COL_POINTER,
	NUM_COLUMNS,	
};
*/
enum
{
        COL_PID  = 0,
        COL_NAME ,
        COL_USER,
        COL_CPU,
        COL_MEM,
        COL_VMSIZE,
        COL_STARTED,
        COL_PPID,
        COL_ARGS,
        //COL_MEMRES,
        //COL_MEMSHARED,
        //COL_MEMRSS,
        COL_PIXBUF,
        COL_POINTER,
        NUM_COLUMNS,
};

GtkWidget*	proctable_new (ProcData *data);
void		proctable_update_table (ProcData *data);
void		proctable_update_list (ProcData *data);
void		insert_info_to_tree (ProcInfo *info, ProcData *procdata);
void		remove_info_from_tree (ProcInfo *info, ProcData *procdata);
ProcInfo *	proctable_find_process (gint pid, gchar *name, ProcData *procdata);
void		proctable_update_all (ProcData *data);
void		proctable_clear_tree (ProcData *data);
void		proctable_free_table (ProcData *data);
void		proctable_search_table (ProcData *procdata, gchar *string);
void 		proctable_free_info (ProcInfo *info);
void		save_selections ();
static gint
sort_ints (GtkTreeModel *model, GtkTreeIter *itera, GtkTreeIter *iterb, gpointer data);
ProcData *procdata;
static void  update_info (ProcInfo *info, struct Process_Data data);

#endif
