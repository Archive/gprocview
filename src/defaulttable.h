#ifndef _DEFAULTTABLE_H_
#define _DEFAULTTABLE_H_

/* This file contains prettynames and icons for well-known applications, that by default has no .desktop entry */

/* The current table is only a test */
const gchar *default_table[] = {
				"X", N_("X window system"), "gnome-mdi.png",
				"bash", N_("bourne again shell"), "gnome-term.png",
				"gnome-session", N_("Gnome-Session"), "gnome-logo-icon-transparent.png",
				NULL};

#endif /* _DEFAULTTABLE_H_ */
