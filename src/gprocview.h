/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef _GPROCVIEW_H
#define _GPROCVIEW_H

#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gnome.h>
#include <gconf/gconf-client.h>
#include <siginfo.h>

typedef struct _ProcConfig ProcConfig;
typedef struct _PrettyTable PrettyTable;
typedef struct _LoadGraph LoadGraph;
typedef struct _ProcInfo ProcInfo;
typedef struct _ProcData ProcData;
#define MAX_FILTER_STRING_SIZE 256
#define MAX_TEMP_BUFFER_SIZE   1024
#define MAX_NUMBER_OF_RECORDS  300
#define MAX_SPACE_BETWEEN_FIELDS 10
enum
{
	ALL_PROCESSES,
	MY_PROCESSES,
	RUNNING_PROCESSES,
	FAVORITE_PROCESSES,
};

#define NCPUSTATES 4
#define MIN_SPACES 8
struct _ProcConfig
{
	gint		width;
	gint		height;
        gboolean        show_more_info;
        gboolean	show_kill_warning;
        gboolean	show_hide_message;
        gboolean	show_tree;
        gboolean	show_threads;
 	guint32		update_interval;
 	//gint		graph_update_interval;
 	//gint		disks_update_interval;
	gint		whose_process;
//	gint		current_tab;
//	GdkColor	cpu_color;
//	GdkColor	mem_color;
//	GdkColor	swap_color;
//	GdkColor	bg_color;
//	GdkColor	frame_color;
	gboolean	simple_view;
	gchar		filter;
//	gint		pane_pos;
};

struct _PrettyTable {
	GHashTable *app_hash;		/* apps gotten from libwnck */      
	GHashTable *default_hash; 	/* defined in defaulttable.h */
};

struct _LoadGraph {
    
    guint n;
    gint type;
    guint speed;
    guint draw_width, draw_height;
    guint num_points;

    guint allocated;

    GdkColor *colors;
    gfloat **data, **odata;
    guint data_size;
    guint *pos;

    gint colors_allocated;
    GtkWidget *main_widget;
    GtkWidget *disp;
    GtkWidget *label;
    GtkWidget *memused_label;
    GtkWidget *memtotal_label;
    GtkWidget *swapused_label;
    GtkWidget *swaptotal_label;
    GdkPixmap *pixmap;
    GdkGC *gc;
    int timer_index;
    
    gboolean draw;

    long cpu_time [NCPUSTATES];
    long cpu_last [NCPUSTATES];
    int cpu_initialized;       
};

struct _ProcInfo
{
	GtkTreeIter 	node;
	GtkTreeIter 	parent_node;
	gboolean	visible;
	gboolean	has_parent;
	GdkPixbuf	*pixbuf;
	gchar		*name;
	gchar		*user;
	gchar		*arguments;
	gint		mem;
	gfloat		cpu;
//	gchar		cpu[32];
	gint		pid;
	gint		parent_pid;
	gint		cpu_time_last;
	//gint		nice;
	gint		vmsize;
	gint		memres;
	gint		memshared;
	gint		memrss;
	gchar		*status;
	gboolean	running;
	gboolean	is_thread;
	gboolean	is_blacklisted;
};

struct _ProcData
{
	GtkWidget	*tree;
	GtkWidget	*infobox;
	GtkWidget	*disk_list;
	GtkWidget	*entry;
	ProcConfig	config;
//	LoadGraph	*cpu_graph;
//	LoadGraph	*mem_graph;
	ProcInfo	*selected_process;
	GtkTreeSelection *selection;
	gint		timeout;
	gint		disk_timeout;
	GList		*info;
	PrettyTable	*pretty_table;
	GList		*blacklist;
	gint		blacklist_num;
	gchar	  	filter [MAX_FILTER_STRING_SIZE];
	GConfClient	*client;
	gint		number_of_records;	
};
struct Process_Data
{
        int  pid;
        char pname [16];
        char puser [16];
        char pcpu [16];
        int  pram;
        int  pvsize;
        char pstime [16];
        int  ppid;
        char pargs [1024];
}Process_Data;
struct Selection_Details
{
	char selection_type;
	char Filter [MAX_FILTER_STRING_SIZE];
	int  Number_Of_Records;
}Selection_Details;

void		gprocview_save_config (ProcData *data);
void		gprocview_save_tree_state (GConfClient *client, GtkWidget *tree, gchar *prefix);
gboolean	gprocview_get_tree_state (GConfClient *client, GtkWidget *tree, gchar *prefix);

extern gboolean gail_up;
gboolean check_gail(GtkWidget *widget);
void add_atk_namedesc(GtkWidget *widget, const gchar *name, const gchar *desc);
void add_atk_relation(GtkWidget *obj1, GtkWidget *obj2, AtkRelationType type);

#endif
