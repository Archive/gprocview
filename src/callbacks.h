/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */
 
#ifndef _CALLBACKS_H_
#define _CALLBACKS_H_

#if 0
#include <gnome.h>
#include <gal/e-table/e-table.h>
#endif
#include "gprocview.h"



void
cb_properties_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data);


void		cb_save_visible (GtkMenuItem *menuitem, gpointer data);
void		cb_log_visible (GtkMenuItem *menuitem, gpointer data);
void		cb_owner_details (GtkMenuItem *menuitem, gpointer data);
void		cb_send_signal (GtkMenuItem *menuitem, gpointer data);
void		cb_debig_clicked (GtkMenuItem *menuitem, gpointer data);
void		cb_trace_system_call (GtkMenuItem *menuitem, gpointer data);
void		cb_trace_children (GtkMenuItem *menuitem, gpointer data);
void		cb_show_stack (GtkMenuItem *menuitem, gpointer data);
void		cb_show_ancestry (GtkMenuItem *menuitem, gpointer data);
void		cb_copy_clicked (GtkMenuItem *menuitem, gpointer data);
void		cb_toolbar_clicked (GtkMenuItem *menuitem, gpointer data);
void		cb_show_columns (GtkMenuItem *menuitem, gpointer data);
void		cb_show_sort_by (GtkMenuItem *menuitem, gpointer data);
void		cb_hide_selected_process (GtkMenuItem *menuitem, gpointer data);
void		cb_sample_now (GtkMenuItem *menuitem, gpointer data);
void		cb_stop_sampling (GtkMenuItem *menuitem, gpointer data);
void		cb_show_all_processes (GtkMenuItem *menuitem, gpointer user_data);
void cb_show_filed_clicked (GtkMenuItem *menuitem, gpointer user_data);

void		cb_show_memory_maps (GtkMenuItem *menuitem, gpointer data);

void		cb_sort_clicked_name (GtkMenuItem *menuitem, gpointer data);
void		cb_sort_clicked_id (GtkMenuItem *menuitem, gpointer data);
void		cb_sort_clicked_owner (GtkMenuItem *menuitem, gpointer data);
void		cb_sort_clicked_cpu (GtkMenuItem *menuitem, gpointer data);
void		cb_sort_clicked_mem (GtkMenuItem *menuitem, gpointer data);
void		cb_sort_clicked_time (GtkMenuItem *menuitem, gpointer data);
void		cb_sort_clicked_parent (GtkMenuItem *menuitem, gpointer data);
void		cb_sort_clicked_cmd (GtkMenuItem *menuitem, gpointer data);
void		cb_sort_clicked_swap (GtkMenuItem *menuitem, gpointer data);

void		cb_debug (GtkMenuItem *menuitem, gpointer data);
void		cb_owner (GtkMenuItem *menuitem, gpointer data);
void		cb_trce_children(GtkMenuItem *menuitem, gpointer data);

void		cb_renice (GtkMenuItem *menuitem, gpointer data);
void		cb_add_to_favorites (GtkMenuItem *menuitem, gpointer data);
void		cb_end_process (GtkMenuItem *menuitem, gpointer data);
void		cb_kill_process (GtkMenuItem *menuitem, gpointer data);
void		cb_hide_process (GtkMenuItem *menuitem, gpointer data);
void		cb_show_hidden_processes (GtkMenuItem *menuitem, gpointer data);
void		cb_preferences_activate (GtkMenuItem *menuitem, gpointer user_data);

void		cb_about_activate (GtkMenuItem *menuitem, gpointer user_data);
void 		cb_filter (GtkEditable *editable, gpointer data);
void cb_select_all (GtkMenuItem *menuitem, gpointer user_data);
void		cb_app_exit (GtkObject *object, gpointer user_data); 
void		cb_app_delete (GtkWidget *window, GdkEventAny *ev, gpointer data);
#if 0
gboolean	cb_close_simple_dialog (GnomeDialog *dialog, gpointer data);
#endif                                        
void		cb_all_process_menu_clicked (GtkWidget *widget, gpointer data);	
					 
void		cb_my_process_menu_clicked (GtkWidget *widget, gpointer data);  
					 
void		cb_running_process_menu_clicked	(GtkWidget *widget, gpointer data);

void		cb_favorites_menu_clicked (GtkWidget *widget, gpointer data);
void		cb_end_process_button_pressed (GtkButton *button, gpointer data);
void		cb_logout (GtkButton *button, gpointer data);

void		popup_menu_renice (GtkMenuItem *menuitem, gpointer data);
void		popup_menu_show_memory_maps (GtkMenuItem *menuitem, gpointer data);
void		popup_menu_hide_process (GtkMenuItem *menuitem, gpointer data);
void 		popup_menu_end_process (GtkMenuItem *menuitem, gpointer data);
void 		popup_menu_kill_process (GtkMenuItem *menuitem, gpointer data);
void 		popup_menu_about_process (GtkMenuItem *menuitem, gpointer data);

void		cb_info_button_pressed (GtkButton *button, gpointer user_data);
void		cb_search (GtkEditable *editable, gpointer data);


void		cb_cpu_color_changed (GnomeColorPicker *cp, guint r, guint g, guint b,
				      guint a, gpointer data);
void		cb_mem_color_changed (GnomeColorPicker *cp, guint r, guint g, guint b,
				      guint a, gpointer data);
void		cb_swap_color_changed (GnomeColorPicker *cp, guint r, guint g, guint b,
				      guint a, gpointer data);

void		cb_row_selected (GtkTreeSelection *selection, gpointer data);
gboolean	cb_tree_button_pressed (GtkWidget *widget, GdkEventButton *event, 
					gpointer data);
gint		cb_tree_key_press (GtkWidget *widget, GdkEventKey *event, gpointer data);

gint		cb_tree_popup_menu (GtkWidget *widget, gpointer data);
					 
void		cb_switch_page (GtkNotebook *nb, GtkNotebookPage *page, 
			        gint num, gpointer data);

gint 		cb_update_disks (gpointer data);
gint		cb_timeout (gpointer data);
gchar *get_size_string (gfloat fsize) ;

#endif
