/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <pwd.h>
#include <signal.h>
#include "gprocview.h"
#include "proctable.h"
#include "callbacks.h"
//#include "prettytable.h"
#include "interface.h"
#include "favorites.h"

#include <siginfo.h>
#include <unistd.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <sys/file.h>
#include <sys/fcntl.h>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include "gprocview.h" 

extern int max_shared_memory_size;
extern char *shmptr;
extern sem_t *semdes;
//extern gboolean WriteRecords;
extern gboolean isSignalPending;

//extern GtkWidget *scrolled;

gint total_time = 0;
gint total_time_last = 0;
gint total_time_meter = 0;
gint total_time_last_meter = 0;
gint cpu_time_last = 0;
gint cpu_time = 0;
gfloat pcpu_last = 0.0;

extern int semid;
//extern pid_t cpid;
//extern struct sembuf op_lock[2] ;
//extern struct sembuf op_unlock[1];
//extern gboolean Process_Executing;
extern char *shared_memory_address;
extern gboolean isCompleteCycleOver;
extern gboolean isStopSamplingSelected;
gboolean isScrollingClicked;

void get_info (gint pid_index , struct Process_Data *data);
void copy_pids (gint *pid_array, gint number_of_records);

static gint
sort_ints (GtkTreeModel *model, GtkTreeIter *itera, GtkTreeIter *iterb, gpointer data)
{
	ProcInfo *infoa = NULL, *infob = NULL;
	gint col = GPOINTER_TO_INT (data);
	gint a, b;
	gfloat a1 , b1;	
	gtk_tree_model_get (model, itera, COL_POINTER, &infoa, -1);
	gtk_tree_model_get (model, iterb, COL_POINTER, &infob, -1);
	g_return_val_if_fail (infoa, 0);
	g_return_val_if_fail (infob, 0);		
	
	switch (col) {
	case COL_MEM:
		a = infoa->mem;
		b = infob->mem;
		break;
	case COL_CPU:
		a1 = infoa->cpu;
		b1 = infob->cpu;
        	if (a1 > b1)
                	return -1;
		else if (a1 < b1)
			return 1;
		else
            		return 0;
		break;
	case COL_PID:
		a = infoa->pid;
		b = infob->pid;
		break;
	case COL_VMSIZE:
		a = infoa->vmsize;
		b = infob->vmsize;
		break;
	default:
		return 0;
		break;
	}	
	
	if (a > b)
		return -1;
	else if (a < b)
		return 1;
        else
                return 0;	
}

GtkWidget *
proctable_new (ProcData *data)
{
	ProcData *procdata = data;
	GtkWidget *proctree;
	GtkTreeStore *model;
	GtkTreeSelection *selection;
	GtkTreeViewColumn *column;
	GtkWidget *scrolled = NULL;
  	GtkCellRenderer *cell_renderer;

        static gchar *title[] = {N_("ID"), N_("Name"),
                                 N_("Owner"), N_("CPU%"),
                                 N_("RAM"), N_("Swap"),
                                 N_("Started"), N_("Parent"),N_("Command"), NULL, "POINTER"};

	gint i;
	
	scrolled = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled),
                                  	GTK_POLICY_AUTOMATIC,
                          	GTK_POLICY_AUTOMATIC);

        model = gtk_tree_store_new (NUM_COLUMNS, G_TYPE_INT, G_TYPE_STRING,
                                    G_TYPE_STRING, G_TYPE_FLOAT,
                                    G_TYPE_INT, G_TYPE_INT,
                                    G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING,
                                    GDK_TYPE_PIXBUF, G_TYPE_POINTER);

  	proctree = gtk_tree_view_new_with_model (GTK_TREE_MODEL (model));
	gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (proctree), TRUE);
  	g_object_unref (G_OBJECT (model));
  	
  	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (proctree));
  	gtk_tree_selection_set_mode (selection, GTK_SELECTION_MULTIPLE);
  	
  	column = gtk_tree_view_column_new ();
  	for (i = 0; i < NUM_COLUMNS - 2; i++) {
  		cell_renderer = gtk_cell_renderer_text_new ();
		if ( (i == COL_CPU) || (i == COL_PID) || (i ==COL_PPID)
	 	     || (i==COL_MEM ) || (i== COL_VMSIZE) ||(i == COL_STARTED) ) {
			/* Set the alignment to the right */
			cell_renderer->xalign = 1;
		}	

		
  		column = gtk_tree_view_column_new_with_attributes (title[i],
						    		   cell_renderer,
						     		   "text", i,
						     		   NULL);
		gtk_tree_view_column_set_sort_column_id (column, i);
		gtk_tree_view_column_set_resizable (column, TRUE);
		gtk_tree_view_append_column (GTK_TREE_VIEW (proctree), column);
		if (i == COL_CPU)
		{
			gtk_tree_view_column_set_sort_indicator (column, TRUE);
			gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
                                              COL_CPU,
                                              GTK_SORT_ASCENDING);
		}
	}
	
	gtk_container_add (GTK_CONTAINER (scrolled), proctree);
	//gtk_tree_view_column_set_sort_column_id (column, COL_CPU);	
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (model),
					 COL_MEM,
					 sort_ints,
					 GINT_TO_POINTER (COL_MEM),
					 NULL);
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (model),
					 COL_CPU,
					 sort_ints,
					 GINT_TO_POINTER (COL_CPU),
					 NULL);
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (model),
					 COL_PID,
					 sort_ints,
					 GINT_TO_POINTER (COL_PID),
					 NULL);
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (model),
					 COL_VMSIZE,
					 sort_ints,
					 GINT_TO_POINTER (COL_VMSIZE),
					 NULL);
	procdata->tree = proctree;
	
	gprocview_get_tree_state (procdata->client, proctree, "/apps/gprocview/proctree");

	g_signal_connect (G_OBJECT (gtk_tree_view_get_selection (GTK_TREE_VIEW (proctree))), 
			  "changed",
			  G_CALLBACK (cb_row_selected), procdata);
	g_signal_connect (G_OBJECT (proctree), "button_press_event",
			  G_CALLBACK (cb_tree_button_pressed), procdata);

	return scrolled;

}



void
proctable_free_info (ProcInfo *info)
{
	if (!info)
		return;
	if (info->name)
		g_free (info->name);
	if (info->arguments)
		g_free (info->arguments);
	if (info->user)
		g_free (info->user);
	if (info->status)
		g_free (info->status);
	if (info->pixbuf)
		g_object_unref (info->pixbuf);
	g_free (info);
}



static void
get_process_status (ProcInfo *info, char *state)
{
	if (info->status)
		g_free (info->status);
		
	if (!g_strcasecmp (state, "r"))
	{
		info->status = g_strdup_printf (_("Running"));
		info->running = TRUE;
		return;
	}
	else if (!g_strcasecmp (state, "t"))
	{
		info->status = g_strdup_printf (_("Stopped"));
		info->running = FALSE;
	}
	else
	{
		info->status = g_strdup_printf (_("Sleeping"));
		info->running = FALSE;
	}

}

static void
get_process_name (ProcData *procdata, ProcInfo *info, gchar *cmd, gchar *args)
{
	gchar *command = NULL;
	gint i, n = 0, len, newlen;
	gboolean done = FALSE;
						  
	/* strip the absolute path from the arguments */	
	if (args)
	{
		len = strlen (args);
		i = len;
		while (!done)
		{
			/* no / in string */
			if (i == 0) {
				n = 0;
				done = TRUE;
			}
			if (args[i] == '/') {
				done = TRUE;
				n = i + 1;
			}
			i--;
		}		
		newlen = len - n;
		command = g_new (gchar, newlen + 1);
		for (i = 0; i < newlen; i++) {
			command[i] = args[i + n];
		}
		command[newlen] = '\0';
	}
	
	if (command)
		info->name = g_strdup (command);
	else if (!command)
		info->name = g_strdup (cmd);
		
	if (command)
		g_free (command);

}

static ProcInfo *
find_parent (ProcData *data, gint pid)
{
	GList *list = data->info;
	
	
	/* ignore init as a parent process */
	if (pid <= 1)
		return NULL;

	while (list)
	{
		ProcInfo *info = list->data;
		
		if (pid == info->pid) 
			return info;
						
		list = g_list_next (list);
	}
	return NULL;
}


void
insert_info_to_tree (ProcInfo *info, ProcData *procdata)
{
	GtkTreeModel *model;
	GtkTreeIter row;
	g_return_if_fail (info);
	if (is_process_blacklisted (procdata, info->name))
	{	
		info->is_blacklisted = TRUE;
		return;
	}
	info->is_blacklisted = FALSE;
	model = gtk_tree_view_get_model (GTK_TREE_VIEW (procdata->tree));
	if (info->has_parent && procdata->config.show_tree) {
		GtkTreePath *parent_node = gtk_tree_model_get_path (model, &info->parent_node);
		
		gtk_tree_store_insert (GTK_TREE_STORE (model), &row, &info->parent_node, 0);
		if (!gtk_tree_view_row_expanded (GTK_TREE_VIEW (procdata->tree), parent_node))     
			gtk_tree_view_expand_row (GTK_TREE_VIEW (procdata->tree),
					  parent_node,
					  FALSE);
					  
		gtk_tree_path_free (parent_node);
	}	
	else
		gtk_tree_store_insert (GTK_TREE_STORE (model), &row, NULL, 0);
	/* COL_POINTER must be set first, because GtkTreeStore
         * will call sort_ints as soon as we set the column
         * that we're sorting on.
         */
/*
	gtk_tree_store_set (GTK_TREE_STORE (model), &row,
                            COL_POINTER, info,
                            COL_NAME, info->name,
                            COL_ARGS, info->arguments,
                            COL_USER, info->user,
                            COL_STARTED, info->status,
                            COL_MEM, info->mem,
                            COL_VMSIZE, info->vmsize,
                            COL_CPU, info->cpu,
			    COL_PPID, info->parent_pid, 
                            COL_PID, info->pid,
                            -1);
*/
        gtk_tree_store_set (GTK_TREE_STORE (model), &row,
                            COL_POINTER, info,
                            COL_PID, info->pid,
                            COL_NAME, info->name,
                            COL_USER, info->user,
                            COL_CPU, info->cpu,
                            COL_MEM, info->mem,
                            COL_VMSIZE, info->vmsize,
                            COL_STARTED, info->status,
                            COL_PPID, info->parent_pid,
                            COL_ARGS, info->arguments,
                            -1);

	info->node = row;
	info->visible = TRUE;
}



/* Kind of a hack. When a parent process is removed we remove all the info
** pertaining to the child processes and then readd them later
*/
static void
remove_children_from_tree (ProcData *procdata, GtkTreeModel *model,
			   GtkTreeIter *parent)
{
	ProcInfo *child_info;
	
	do {
		GtkTreeIter child;
		
		if (gtk_tree_model_iter_children (model, &child, parent))
			remove_children_from_tree (procdata, model, &child);
		
		gtk_tree_model_get (model, parent, COL_POINTER, &child_info, -1);
		if (child_info) {
			procdata->info = g_list_remove (procdata->info, child_info);
			proctable_free_info (child_info);
		}
	} while (gtk_tree_model_iter_next (model, parent));
		
}

void
remove_info_from_tree (ProcInfo *info, ProcData *procdata)
{
	GtkTreeModel *model;
	GtkTreeIter iter, child;
	GtkTreePath *node;
	gboolean selected = FALSE;
	
	g_return_if_fail (info);
	
	if (!info->visible)
		return;
	
	model = gtk_tree_view_get_model (GTK_TREE_VIEW (procdata->tree));
	iter = info->node;

	g_return_if_fail (&iter);

/*	
	if (gtk_tree_model_iter_children (model, &child, &iter))
		remove_children_from_tree (procdata, model, &child);
*/	
	if (procdata->selected_process == info) 
		selected = TRUE;
	
	gtk_tree_store_remove (GTK_TREE_STORE (model), &iter);
	
	if (selected) {
		node = gtk_tree_model_get_path (model, &iter);
		gtk_tree_view_set_cursor (GTK_TREE_VIEW (procdata->tree), node, NULL, FALSE);
		gtk_tree_path_free (node);
	}
	
	info->visible = FALSE;	
		
}


ProcInfo *
proctable_find_process (gint pid, gchar *name, ProcData *procdata)
{

	GList *list = procdata->info;
	
	while (list) {
		ProcInfo *info = list->data;
		
		if (pid == info->pid)
			return info;
		
		if (name) {	
			if (g_strcasecmp (name, info->name) == 0)
				return info;
		}
		
		list = g_list_next (list);
	}

	return NULL;
}


void
proctable_update_list (ProcData *data)
{
/*	ProcData *procdata = data;
	unsigned *pid_list;
	glibtop_proclist proclist;
	glibtop_cpu cpu;
	gint which, arg;
	gint n;
	
	
	switch (procdata->config.whose_process) {
	case ALL_PROCESSES:
	case RUNNING_PROCESSES:
	case FAVORITE_PROCESSES:
		which = GLIBTOP_KERN_PROC_ALL;
		arg = 0;
		break;
	default:
		which = GLIBTOP_KERN_PROC_UID;
		arg = getuid ();
		break;
	}
	

	pid_list = glibtop_get_proclist (&proclist, which, arg);
	n = proclist.number;
	
	glibtop_get_cpu (&cpu);
	total_time = cpu.total - total_time_last;
	total_time_last = cpu.total;
	
	//refresh_list (procdata, pid_list, n);
	new_refresh_list (procdata);
	glibtop_free (pid_list);
*/	
     	ProcData *procdata = data;
	gint line,n;
	unsigned *process_list;
	FILE *FilePtr = NULL;
	char temp[1024] = {0};
	char *psstr = "/usr/bin/ps -ef | wc -l "; //sprintf ("ps -U %d", getuid ());
	char *psstr1 = "/usr/bin/ps -A -o pid=ID";
 	FilePtr = popen (psstr,"r");
        fgets (temp, 1024,FilePtr);	
	line = atoi(temp)- 1;
	pclose (FilePtr);
	process_list = malloc (sizeof(unsigned int) * line);
	FilePtr = popen (psstr1, "r");
	
		fgets (temp,1024,FilePtr);
	for (n = 1; n < line; n++)
	{
		fgets (temp,1024,FilePtr);
	 	process_list[n-1] = atol (temp); 	
	}
	pclose (FilePtr)	;
        //refresh_list (procdata, process_list, line);
	//new_refresh_list_1 (procdata);
}

/**
 * Reader_Func :
 *
 * Description:
 *  	This function will read shared memory i.e. number of records
 * 	and will those many records. 
 **/
void Reader_Func ()	
{
  int shmdes, index,fd, shar_mem_size, fde,number_of_records; 
  char *strptr, process_selection;
  FILE *FilePtr = NULL;	
  ProcInfo *info = NULL;
  struct stat st1 ;
  GList *list = procdata->info;
  int shmid;	 	
  int pid [1024] = {0};	


	strptr = shared_memory_address;
	number_of_records = procdata->number_of_records;

	// Copy the pids of all proceses in an array and use get_info function to update the content for the same
	copy_pids (pid, number_of_records);


	index = 0;
	while ( index < number_of_records)
	{
		ProcInfo *oldinfo;

                // New process with higher pid than any previous one 
                if (!list)
                {
                        ProcInfo *info;
                	struct Process_Data data;
			info = g_new0 (ProcInfo, 1);
			get_info (index, &data);
                	info->arguments = g_strdup (data.pargs);
                	info->name = g_strdup(data.pname);
                	info->user = g_strdup (data.puser);
                	info->status = g_strdup (data.pstime);
                	info->mem = data.pram;
                	info->vmsize = data.pvsize;
                	info->cpu = atof(data.pcpu);
                	info->pid = data.pid;
                	info->parent_pid = data.ppid;

                        insert_info_to_tree (info, procdata);
                        procdata->info = g_list_append (procdata->info, info);


                        index++;
                        continue;
                }
                else {
                        oldinfo = list->data;  
		}

                // new process 
                if (pid[index] < oldinfo->pid)
                {
                        ProcInfo *info;
                	struct Process_Data data;
                       	info = g_new0 (ProcInfo, 1);
			get_info (index, &data);
                        info->arguments = g_strdup (data.pargs);
                        info->name = g_strdup(data.pname);
                        info->user = g_strdup (data.puser);
                        info->status = g_strdup (data.pstime);
                        info->mem = data.pram;
                        info->vmsize = data.pvsize;
                        info->cpu = atof(data.pcpu);
                        info->pid = data.pid;
                        info->parent_pid = data.ppid;

                        insert_info_to_tree (info, procdata);
                        procdata->info = g_list_insert (procdata->info, info, index);
                        index++;
                }
                // existing process 
                else if (pid [index] == oldinfo->pid)
                {
                	struct Process_Data data;
			get_info (index, &data);
                        //update_info (oldinfo, data);
                        list = g_list_next (list);
                        index++;
                }
                // process no longer exists 
                else if (pid [index] > oldinfo->pid)
                {      
                        remove_info_from_tree (oldinfo, procdata);
                        list = g_list_next (list);
                        procdata->info = g_list_remove (procdata->info, oldinfo);
                        proctable_free_info (oldinfo);	
                }
	} 
	
        // Get rid of any tasks at end of list that have ended 
        while (list)
        {
                ProcInfo *oldinfo;
                oldinfo = list->data;
                remove_info_from_tree (oldinfo, procdata);
                list = g_list_next (list);
                procdata->info = g_list_remove (procdata->info, oldinfo);
                proctable_free_info (oldinfo);
        }

	update_sensitivity_controls (TRUE);
	//Process_Executing = FALSE;
	if (isCompleteCycleOver == FALSE)
		isCompleteCycleOver = TRUE;
	if (isSignalPending)
		isSignalPending = FALSE;

}

void
proctable_update_all (ProcData *data)
{
	ProcData *procdata = data;
/*	
	proctable_update_list (procdata);
	
	if (procdata->config.show_more_info)
		infoview_update (procdata);
*/
	//proctable_clear_tree (procdata);
	//WriteRecords = TRUE;
}

void 
proctable_clear_tree (ProcData *data)
{
	ProcData *procdata = data;
	GtkTreeModel *model;
	
	model = gtk_tree_view_get_model (GTK_TREE_VIEW (procdata->tree));
	
	gtk_tree_store_clear (GTK_TREE_STORE (model));
	
	proctable_free_table (procdata);
	
	update_sensitivity (procdata, FALSE);
}

void		
proctable_free_table (ProcData *procdata)
{
	GList *list = procdata->info;
	
	while (list)
	{
		ProcInfo *info = list->data;
		proctable_free_info (info);
		list = g_list_next (list);
	}
	
	g_list_free (procdata->info);
	procdata->info = NULL;
	
}

void
proctable_search_table (ProcData *procdata, gchar *string)
{
	GList *list = procdata->info;
	GtkWidget *dialog;
	GtkTreeModel *model;
	gchar *error;
	static gint increment = 0, index;
	static gchar *last = NULL;
	
	if (!g_strcasecmp (string, ""))
		return;
	
	model = gtk_tree_view_get_model (GTK_TREE_VIEW (procdata->tree));
	
	if (!last)
		last = g_strdup (string);
	else if (g_strcasecmp (string, last)) {
		increment = 0;
		g_free (last);
		last = g_strdup (string);
	}
	else 
		increment ++;
	
	index = increment;
	
	while (list)
	{
		ProcInfo *info = list->data;
		if (strstr (info->name, string) && info->visible)
		{
			GtkTreePath *node = gtk_tree_model_get_path (model, &info->node);
			
			if (index == 0) {
				gtk_tree_view_set_cursor (GTK_TREE_VIEW (procdata->tree), 
							 node,
							 NULL,
							 FALSE);
				return;
			}
			else
				index --;
				
			gtk_tree_path_free (node);
		}
		
		if (strstr (info->user, string) && info->visible)
		{
			GtkTreePath *node = gtk_tree_model_get_path (model, &info->node);
			
			if (index == 0) {
				gtk_tree_view_set_cursor (GTK_TREE_VIEW (procdata->tree), 
							 node,
							 NULL,
							 FALSE);
				return;
			}
			else
				index --;
				
			gtk_tree_path_free (node);
		}
		
		list = g_list_next (list);
	}
	
	if (index == increment) {
		error = g_strdup_printf (_("%s could not be found."), string);
		dialog = gtk_message_dialog_new (NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
                                  		 GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
                                  		 "%s", error, NULL); 
		gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy (dialog);
		g_free (error);
	}
	else {
		/* no more cases found. Start the search anew */
		increment = -1;
		proctable_search_table (procdata, string);
		return;
	}
	
	increment --;

}

static void
update_info (ProcInfo *info, struct Process_Data data)
{
	GtkTreeModel *model;
        model = gtk_tree_view_get_model (GTK_TREE_VIEW (procdata->tree));
        if (info->visible) {
	float f_cpu = atof (data.pcpu);
//	if ((info->mem != data.pram) || ((info->cpu - f_cpu) > 0.0) || (info->vmsize != data.pvsize))
        gtk_tree_store_set (GTK_TREE_STORE (model), &info->node,
                            COL_POINTER, info,
                            COL_MEM, info->mem,
                            COL_VMSIZE, data.pvsize,
                            COL_CPU, info->cpu,
                            -1);

        }

}

void get_info (gint pid_index , struct Process_Data *data)
{
	gint Record_Index = pid_index * sizeof (Process_Data);
	memcpy (data, (shared_memory_address + Record_Index), sizeof (Process_Data));	
}

void copy_pids (gint *pid_array, gint number_of_records)
{
	struct Process_Data pdata;
	char *tempptr = shared_memory_address; 	
	gint rec_index = 0, arr_index = 0;	
	for (rec_index = 0; rec_index < number_of_records; rec_index++)
	{
		memcpy (&pdata, tempptr, sizeof (Process_Data));
		pid_array [arr_index++]	 = pdata.pid; 
		tempptr += sizeof (Process_Data);	
	}
 
}
