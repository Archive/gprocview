/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <config.h>
#include <glib.h>	
#include <gnome.h>
#include <libgnomeui/gnome-window-icon.h>
#include <gconf/gconf-client.h>
#include <pthread.h>
#include "gprocview.h"
#include "interface.h"
#include "proctable.h"
//#include "prettytable.h"
#include "favorites.h"
#include "callbacks.h"
#include <unistd.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <sys/file.h>
#include <sys/fcntl.h>
#include <semaphore.h>
#include <signal.h> 
#include <poll.h>
#include <pwd.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>


G_CONST_RETURN char *cmdstr =  "/usr/bin/ps -o pid=ID -o 'fname=Name' -o user=Owner -o 'pcpu=%CPU' -o 'rss=RAM' -o 'vsz=Size' -o 'stime=Started' -o ppid=xParent -o 'args=Command' ";

char *shar_mem = NULL;
char *shared_memory_address = NULL;
char *shmptr = NULL;
char *shared_memory_ptr = NULL;
GtkWidget *app;
static int simple_view = FALSE;
pthread_t thread;
int shmid;
int semid;

// This program will display only MAX_NUMBER_OF_RECORDS number of processes only.
int max_shared_memory_size = (sizeof (Process_Data) * MAX_NUMBER_OF_RECORDS); // + sizeof (Process_Data);

ProcData *procdata = NULL;
gboolean WriteRecords= FALSE;
gboolean User_Selection_Changed= FALSE;

gboolean gail_up;

void Reader_Func ();
void Writer_Func ();


struct poptOption options[] = {
  {
    "simple",
    's',
    POPT_ARG_NONE,
    &simple_view,
    0,
    N_("show simple dialog to end processes and logout"),
    NULL,
  },
  {
    NULL,
    '\0',
    0,
    NULL,
    0,
    NULL,
    NULL
  }
};

static void
load_desktop_files (ProcData *pd)
{
	pd->pretty_table = pretty_table_new (pd);
}

static void
tree_changed_cb (GConfClient *client, guint id, GConfEntry *entry, gpointer data)
{
	ProcData *procdata = data;
	GConfValue *value = gconf_entry_get_value (entry);
	
	procdata->config.show_tree = gconf_value_get_bool (value);
	proctable_clear_tree (procdata);
	proctable_update_all (procdata);
	
}

static void
threads_changed_cb (GConfClient *client, guint id, GConfEntry *entry, gpointer data)
{
	ProcData *procdata = data;
	GConfValue *value = gconf_entry_get_value (entry);
	
	procdata->config.show_threads = gconf_value_get_bool (value);
	proctable_clear_tree (procdata);
	proctable_update_all (procdata);
	
}

static void
view_as_changed_cb (GConfClient *client, guint id, GConfEntry *entry, gpointer data)
{
	ProcData *procdata = data;
	GConfValue *value = gconf_entry_get_value (entry);
	
	procdata->config.whose_process = gconf_value_get_int (value);
	procdata->config.whose_process = CLAMP (procdata->config.whose_process, 0, 2);
	proctable_clear_tree (procdata);
	//TODOproctable_update_all (procdata);
}

static void
warning_changed_cb (GConfClient *client, guint id, GConfEntry *entry, gpointer data)
{
	ProcData *procdata = data;
	const gchar *key = gconf_entry_get_key (entry);
	GConfValue *value = gconf_entry_get_value (entry);
	
	if (!g_strcasecmp (key, "/apps/gprocview/kill_dialog")) {
		procdata->config.show_kill_warning = gconf_value_get_bool (value);
	}
	else {
		procdata->config.show_hide_message = gconf_value_get_bool (value);
	}
}

static void
timeouts_changed_cb (GConfClient *client, guint id, GConfEntry *entry, gpointer data)
{
	ProcData *procdata = data;
	const gchar *key = gconf_entry_get_key (entry);
	GConfValue *value = gconf_entry_get_value (entry);

	if (!g_strcasecmp (key, "/apps/gprocview/update_interval")) {
		procdata->config.update_interval = gconf_value_get_int (value);
		procdata->config.update_interval = 
			MAX (procdata->config.update_interval, 1000);
		gtk_timeout_remove (procdata->timeout);
		procdata->timeout = gtk_timeout_add (procdata->config.update_interval, 
						     cb_timeout, procdata);
	}
}
static ProcData *
gprocview_data_new (GConfClient *client)
{

	ProcData *pd;
	gchar *color;
	gint swidth, sheight;
	
	pd = g_new0 (ProcData, 1);
	
	pd->tree = NULL;
	pd->infobox = NULL;
	pd->info = NULL;
	pd->selected_process = NULL;
	pd->timeout = -1;
	pd->blacklist = NULL;
	pd->disk_timeout = -1;

	pd->config.width = gconf_client_get_int (client, "/apps/gprocview/width", NULL);
	pd->config.height = gconf_client_get_int (client, "/apps/gprocview/height", NULL);
	pd->config.show_more_info = gconf_client_get_bool (client, "/apps/gprocview/more_info", NULL);
	pd->config.show_tree = gconf_client_get_bool (client, "/apps/gprocview/show_tree", NULL);
	gconf_client_notify_add (client, "/apps/gprocview/show_tree", tree_changed_cb,
				 pd, NULL, NULL);
	pd->config.show_kill_warning = gconf_client_get_bool (client, "/apps/gprocview/kill_dialog", 
							      NULL);
	gconf_client_notify_add (client, "/apps/gprocview/kill_dialog", warning_changed_cb,
				 pd, NULL, NULL);
	pd->config.show_hide_message = gconf_client_get_bool (client, "/apps/gprocview/hide_message",
							      NULL);
	gconf_client_notify_add (client, "/apps/gprocview/hide_message", warning_changed_cb,
				 pd, NULL, NULL);
	pd->config.show_threads = gconf_client_get_bool (client, "/apps/gprocview/show_threads", NULL);
	gconf_client_notify_add (client, "/apps/gprocview/show_threads", threads_changed_cb,
				 pd, NULL, NULL);
	pd->config.update_interval = gconf_client_get_int (client, "/apps/gprocview/update_interval", 
							   NULL);
	gconf_client_notify_add (client, "/apps/gprocview/update_interval", timeouts_changed_cb,
				 pd, NULL, NULL);
	pd->config.whose_process = gconf_client_get_int (client, "/apps/gprocview/view_as", NULL);
	gconf_client_notify_add (client, "/apps/gprocview/view_as", view_as_changed_cb,
				 pd, NULL, NULL);
	get_blacklist (pd, client);

	pd->config.simple_view = FALSE;	
	if (pd->config.simple_view) {
		pd->config.width = 325;
		pd->config.height = 400;
		pd->config.whose_process = 1;
		pd->config.show_more_info = FALSE;
		pd->config.show_tree = FALSE;
		pd->config.show_kill_warning = TRUE;
		pd->config.show_threads = FALSE;
		//pd->config.current_tab = 0;
	}	
	
	/* Sanity checks */
	swidth = gdk_screen_width ();
	sheight = gdk_screen_height ();
	pd->config.width = CLAMP (pd->config.width, 50, swidth);
	pd->config.height = CLAMP (pd->config.height, 50, sheight);
	//pd->config.update_interval = MAX (pd->config.update_interval, 1000);
	pd->config.update_interval = MAX (pd->config.update_interval, 30000);
	//pd->config.update_interval = 30000;
	pd->config.whose_process = CLAMP (pd->config.whose_process, 0, 2);

	return pd;

}

static void
gprocview_free_data (ProcData *procdata)
{

	proctable_free_table (procdata);
	
	//pretty_table_free (procdata->pretty_table);
	
	g_free (procdata);
	
}


gboolean
gprocview_get_tree_state (GConfClient *client, GtkWidget *tree, gchar *prefix)
{
	GtkTreeModel *model;
	gint sort_col;
	GtkSortType order;
	gchar *key;
	gint i = 0;
	gboolean done = FALSE;
	
	g_return_val_if_fail (tree, FALSE);
	g_return_val_if_fail (prefix, FALSE);
	
	if (!gconf_client_dir_exists (client, prefix, NULL)) 
		return FALSE;
	
	model = gtk_tree_view_get_model (GTK_TREE_VIEW (tree));
	
	key = g_strdup_printf ("/sort_col", prefix);
	sort_col = gconf_client_get_int (client, key, NULL);
	g_free (key);
	
	key = g_strdup_printf ("/sort_order", prefix);
	order = gconf_client_get_int (client, key, NULL);
	g_free (key);
	
	if (sort_col != -1)
		gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
					      	      sort_col,
					              order);
	
	while (!done) {
		GtkTreeViewColumn *column;
		GConfValue *value = NULL;
		gint width;
		gboolean visible;
		
		
		key = g_strdup_printf ("/col_%d_width", prefix, i);
		value = gconf_client_get (client, key, NULL);
		g_free (key);
		
		if (value != NULL) {
			width = gconf_value_get_int(value);
			gconf_value_free (value);
		
			key = g_strdup_printf ("/col_%d_visible", prefix, i);
			visible = gconf_client_get_bool (client, key, NULL);
			g_free (key);
		
			column = gtk_tree_view_get_column (GTK_TREE_VIEW (tree), i);
			gtk_tree_view_column_set_visible (column, visible);
			if (width > 0)
				gtk_tree_view_column_set_fixed_width (column, width);
		}
		else
			done = TRUE;
		
		i++;
	}
	
	return TRUE;
}

void
gprocview_save_tree_state (GConfClient *client, GtkWidget *tree, gchar *prefix)
{
	GtkTreeModel *model;
	GList *columns;
	gint sort_col;
	GtkSortType order;
	gint i = 0;
	
	//g_return_if_fail (tree);
	//g_return_if_fail (prefix);
	
	model = gtk_tree_view_get_model (GTK_TREE_VIEW (tree));
	if (gtk_tree_sortable_get_sort_column_id (GTK_TREE_SORTABLE (model), &sort_col,
					          &order)) {
		gchar *key;
		
		key = g_strdup_printf ("/sort_col", prefix);
		gconf_client_set_int (client, key, sort_col, NULL);
		g_free (key);
		
		key = g_strdup_printf ("/sort_order", prefix);
		gconf_client_set_int (client, key, order, NULL);
		g_free (key);
	}			       
	
	columns = gtk_tree_view_get_columns (GTK_TREE_VIEW (tree));
	
	while (columns) {
		GtkTreeViewColumn *column = columns->data;
		gboolean visible;
		gint width;
		gchar *key;
		
		visible = gtk_tree_view_column_get_visible (column);
		width = gtk_tree_view_column_get_width (column);
		
		key = g_strdup_printf ("/col_%d_width", prefix, i);
		gconf_client_set_int (client, key, width, NULL);
		g_free (key);
		
		key = g_strdup_printf ("/col_%d_visible", prefix, i);
		gconf_client_set_bool (client, key, visible, NULL);
		g_free (key);
		
		columns = g_list_next (columns);
		i++;
	}
	
}

void
gprocview_save_config (ProcData *data)
{
	GConfClient *client = data->client;
	gint width, height, pane_pos;

	if (!data)
		return;
		
	if (data->config.simple_view)
		return;
		
		
	gdk_drawable_get_size (app->window, &width, &height);
	data->config.width = width;
	data->config.height = height;
	
	//pane_pos = get_sys_pane_pos ();
	//data->config.pane_pos = pane_pos;
	
	gconf_client_set_int (client, "/apps/gprocview/width", data->config.width, NULL);
	gconf_client_set_int (client, "/apps/gprocview/height", data->config.height, NULL);	
	gconf_client_set_bool (client, "/apps/gprocview/more_info", data->config.show_more_info, NULL);
	//gconf_client_set_int (client, "/apps/gprocview/current_tab", data->config.current_tab, NULL);
	//gconf_client_set_int (client, "/apps/gprocview/pane_pos", data->config.pane_pos, NULL);
	gconf_client_set_int (client, "/apps/gprocview/gview_as", procdata->config.whose_process, NULL);	
	save_blacklist (data, client);

	gconf_client_suggest_sync (client, NULL);
	

	
}

/**
 * extract_fields 
 * @temp input string from which we want to extract the fields.
 * Description:
 *	This function will extract fields fromt the input string and will 
 *  	put in the Process_Data structure at the same time this function
 * 	will increment the shared memory pointer. 
 **/
gboolean extract_fields (char *temp, int *p_id)
{
        int t1 = 0, spaces_cnt = 0;
        gchar *temp_ptr = NULL, *end_char = NULL, *first_ptr = NULL;
        gchar backup_buffer [MAX_TEMP_BUFFER_SIZE] = {0} ;
        struct Process_Data pdata = {0};
        g_stpcpy (backup_buffer, temp);
	g_stpcpy (temp, backup_buffer);

	end_char = strstr (temp, "\n");
        temp_ptr= strtok (temp , " ");
	first_ptr = temp_ptr;
	if (isdigit (temp_ptr[0]) == FALSE)
		return FALSE;	
        pdata.pid = atol (temp_ptr);
	*p_id = pdata.pid;
        temp_ptr= strtok (NULL, " ");
	//To identify defunc processes
	if ((temp_ptr - first_ptr) > MAX_SPACE_BETWEEN_FIELDS)
		return	FALSE;

        g_stpcpy (pdata.pname, temp_ptr);
        temp_ptr= strtok (NULL, " ");
        g_stpcpy (pdata.puser, temp_ptr);
        temp_ptr= strtok (NULL, " ");
        g_stpcpy (pdata.pcpu, temp_ptr);
        temp_ptr= strtok (NULL, " ");
        pdata.pram = atol (temp_ptr);
        temp_ptr= strtok (NULL, " ");
        pdata.pvsize = atol (temp_ptr);
        temp_ptr= strtok (NULL, " ");
        g_stpcpy (pdata.pstime,temp_ptr);
        temp_ptr= strtok (NULL, " ");
        pdata.ppid = atol (temp_ptr);
        temp_ptr = strtok (NULL, " ");
	if (end_char != NULL)
        	memcpy (pdata.pargs, backup_buffer+ (temp_ptr-temp),end_char-temp_ptr);
	//printf("pdata.args:%d:%s:\n", pdata.pid, pdata.pargs);
        memcpy (shared_memory_ptr,&pdata, sizeof (Process_Data));
        shared_memory_ptr += sizeof (Process_Data);
	return TRUE;
}

/**
 * Writer_Func:
 *
 * Description:
 * This function will read shared memory to filter the "ps" command and will write 
 * the processes data in the shared memory. 
 **/
void Writer_Func ()
{
	FILE *FilePtr = NULL;
        char *ptr = NULL, temp[MAX_TEMP_BUFFER_SIZE] = {0}, 
		temp_cmdstr [MAX_TEMP_BUFFER_SIZE]= {0};
        char *ptr_start = NULL, *ptr_end = NULL;
	char temp_1_buff [1024] ={0};
        //struct stat st1 ;
        int fde,record_size = 0, t1 = 0, record_index = 0, cnt1;
	char process_selection;
	struct passwd *pwd;
	//struct Selection_Details selection_details;
	int shmid;
	FILE *temp_file_ptr = NULL;
	int ProcID [1024] = {0};
	int p_id;
	char *next_ptr, *swap_ptr;	
	struct Process_Data temp_swap_data;
	int temp_swap,i,j ;
	//gchar selectionfilename [MAX_TEMP_BUFFER_SIZE] = {0};

	/* Get the shared memory pointer */ 
	if (shared_memory_address != NULL)
		shared_memory_ptr = shared_memory_address;
	else
	{
		printf ("shared Memory problem \n");
		exit (1);
	}
		switch (procdata->config.whose_process)
		{
        		case ALL_PROCESSES:
				strcpy (temp_cmdstr, cmdstr);
				strcat (temp_cmdstr, " -A ");
                		break;
        		case MY_PROCESSES:
				pwd  = getpwuid (getuid ());
                		sprintf (temp, " -U %s", pwd->pw_name); 
				strcpy (temp_cmdstr, cmdstr);
				strcat (temp_cmdstr, temp);
                		break;
			default:
				strcpy (temp_cmdstr, cmdstr);
				strcat (temp_cmdstr, " -A ");
                		break;
		}
		if ( (procdata->filter != NULL) && (strlen (procdata->filter) > 0) )
		{	
			char filter_string [MAX_FILTER_STRING_SIZE];
			sprintf (filter_string, " | grep %s ", procdata->filter); 
			strcat (temp_cmdstr, filter_string);
		}	
		FilePtr = popen (temp_cmdstr, "r");
		if (FilePtr == NULL)
		{
			shmdt (shared_memory_address);
			printf ("File open error \n");
			exit (1);
		}
	
		// if user gives filter option in that case output of ps will not contaion
		// headers so check for the grep in the command.
		/* if ps command contains "grep" then output will not contain headers for the same
	 	*/
		if (strstr (temp_cmdstr, "grep") == NULL)
			fgets (temp,1024, FilePtr);
		record_index = 0;	
        	while ( (fgets(temp,1024,FilePtr)!= NULL) &&
			(record_index < MAX_NUMBER_OF_RECORDS))
        	{
                	if (extract_fields (temp, &p_id))
			{
				ProcID [record_index] = p_id;
                		record_index++;
			}
        	}

		shared_memory_ptr = shared_memory_address;

		procdata->number_of_records = record_index;
		/* Store the number of records in shared memory */
		//selection_details.Number_Of_Records = record_index;
	
		//memcpy (shared_memory_ptr, &selection_details, sizeof(Selection_Details));
	
	 	swap_ptr = shared_memory_address ;//+ sizeof(Selection_Details);		
		for ( i = 0; i <record_index - 1; i++)
		{
			next_ptr = swap_ptr + sizeof (Process_Data);	
			for (j = i+1; j < record_index; j++)
			{
				if (ProcID [i] > ProcID [j])
				{
					temp_swap = ProcID [i]; ProcID [i] = ProcID [j];
					ProcID [j] = temp_swap;
					memcpy (&temp_swap_data, swap_ptr, sizeof(Process_Data)); 
					memcpy (swap_ptr, next_ptr, sizeof(Process_Data));
					memcpy (next_ptr, &temp_swap_data, sizeof (Process_Data));
				}
				next_ptr += sizeof (Process_Data);
			}
			swap_ptr += sizeof (Process_Data);
		}
	
		pclose (FilePtr);
	if (User_Selection_Changed) 
	{
		User_Selection_Changed = FALSE;
	}

}

/* Check if gail is loaded */
gboolean
check_gail(GtkWidget *widget)
{
	g_return_val_if_fail (GTK_IS_WIDGET(widget), FALSE);
	return GTK_IS_ACCESSIBLE(gtk_widget_get_accessible(widget));
}

/* Add AtkName and AtkDescription */
void
add_atk_namedesc(GtkWidget *widget, const gchar *name, const gchar *desc)
{
	AtkObject *atk_widget;

	g_return_if_fail (GTK_IS_WIDGET(widget));
	atk_widget = gtk_widget_get_accessible(widget);

	if (name)
		atk_object_set_name(atk_widget, name);
	if (desc)
		atk_object_set_description(atk_widget, desc);
}

/* Add AtkRelation */
void
add_atk_relation(GtkWidget *obj1, GtkWidget *obj2, AtkRelationType type)
{

	AtkObject *atk_obj1, *atk_obj2;
	AtkRelationSet *relation_set;
	AtkRelation *relation;

	g_return_if_fail (GTK_IS_WIDGET(obj1));
	g_return_if_fail (GTK_IS_WIDGET(obj2));

	atk_obj1 = gtk_widget_get_accessible(obj1);
	atk_obj2 = gtk_widget_get_accessible(obj2);

	relation_set = atk_object_ref_relation_set (atk_obj1);
	relation = atk_relation_new(&atk_obj2, 1, type);
	atk_relation_set_add(relation_set, relation);
	g_object_unref(G_OBJECT (relation));

}

int
main (int argc, char *argv[])
{
	GnomeProgram *gprocview;
	GConfClient *client;
	GValue value = {0,};
	poptContext pctx;
	char **args;
	int ret;
	key_t shmkey;
	struct shmid_ds shm_struct;
	GTimeVal cur_time;
#ifdef ENABLE_NLS
	bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif
	gprocview = gnome_program_init ("gprocview", VERSION, LIBGNOMEUI_MODULE, argc, argv, 
			    	      GNOME_PARAM_POPT_TABLE, options, 
				      GNOME_PARAM_APP_DATADIR,DATADIR,NULL);
	gconf_init (argc, argv, NULL);
			    
	client = gconf_client_get_default ();
	gconf_client_add_dir(client, "/apps/gprocview", GCONF_CLIENT_PRELOAD_NONE, NULL);
        		    
	/*g_value_init (&value, G_TYPE_POINTER);
  	g_object_get_property (G_OBJECT(),
                               GNOME_PARAM_POPT_CONTEXT, &value);
        (poptContext)pctx = g_value_get_pointer (&value);
                               
	args = (char**) poptGetArgs (pctx);
	poptFreeContext(pctx);*/

	procdata = gprocview_data_new (client);
	procdata->client = client;
	
	if (procdata->config.simple_view) 
		app = create_simple_view_dialog (procdata);
	else 
		app = create_main_window (procdata);
	
	load_desktop_files (procdata);
	g_get_current_time (&cur_time);
	srand (cur_time.tv_usec); 
	shmkey = rand ();	
	shmid = shmget (shmkey, max_shared_memory_size, 0666| IPC_CREAT);
       if (shmid == -1)
	{
       		printf ("shmget failed %d \n", errno);
		exit(1);
	}
 	shared_memory_address = shmat (shmid, (void *)0, 0);
       if (shared_memory_address == (void *) -1)
	{
 		struct shmid_ds shm_struct;
        		shm_struct.shm_segsz = max_shared_memory_size;
			shm_struct.shm_cpid = getppid ();
				shmctl (shmid, IPC_RMID, &shm_struct);
        		printf (" shmat errror %d \n", errno);
			exit (1);
	}
	
		
		//save_selections ();
		Writer_Func ();
		Reader_Func ();

		if (!app) return 0;  

 		gtk_widget_show (app);	

		gtk_main ();

		
		shm_struct.shm_segsz = max_shared_memory_size;
		shm_struct.shm_cpid = getppid ();
        	if (shmctl (shmid, IPC_RMID, &shm_struct) < 0)
		{
         		printf ("shmctl error %d \n", errno);
			exit (1);
		}
		
		gprocview_free_data (procdata);
	
	return 0;
}
