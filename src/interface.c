/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <gdk/gdkkeysyms.h>
#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>
#include <sys/systeminfo.h>
#include "gprocview.h"
#include "callbacks.h"
#include "interface.h"
#include "proctable.h"
//#include "infoview.h"
#include "procactions.h"
#if 0
//#include "prettytable.h"
#include "procdialogs.h"
#include "favorites.h"
#endif
#define MAX_INTERVAL 4294967
#define SAMPLE_TREE(tree) \
	{ GNOME_APP_UI_SUBTREE_STOCK, N_("_Sample"), NULL, tree, NULL, NULL,\
		(GnomeUIPixmapType) 0, NULL, 0, (GdkModifierType) 0, NULL }
extern gboolean isStopSamplingSelected;

static void
update_update_interval (GtkWidget *widget, gpointer data)
{
        ProcData *procdata = data;
        GConfClient *client = procdata->client;
        GtkWidget *spin_button;
        guint32 value;
 
        spin_button = widget; 
        //value = gtk_spin_button_get_value (GTK_SPIN_BUTTON (spin_button));
	value = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (spin_button));
/*	if (value > MAX_INTERVAL)
		value = MAX_INTERVAL;
	if (value < 1)
		value = 1;
*/
        if (1000 * value == procdata->config.update_interval)
                return;
	procdata->config.update_interval = value * 1000; 
        gtk_timeout_remove (procdata->timeout);
	gtk_spin_button_set_value (GTK_SPIN_BUTTON(spin_button), value);
         procdata->timeout = gtk_timeout_add (procdata->config.update_interval,
                                                     cb_timeout, procdata);

	//printf ("Update Func: update interval %d\n", procdata->config.update_interval); 
        gconf_client_set_int (client, "/apps/gprocview/update_interval", value * 1000, NULL);
 
}  
/*
static GnomeUIInfo show_columns1_menu_uiinfo[] =
{
        {
          GNOME_APP_UI_TOGGLEITEM, N_("_Owner"), NULL, cb_show_filed_clicked, NULL, NULL, 0, 0, 0, GDK_CONTROL_MASK
        },
        GNOMEUIINFO_SEPARATOR, 
        {
          GNOME_APP_UI_TOGGLEITEM, N_("_CPU %"), N_("Change the importance (nice value) of a process"),
         cb_save_visible, NULL, NULL, 0, 0,
         0, GDK_CONTROL_MASK
        },
        {
          GNOME_APP_UI_TOGGLEITEM, N_("_Memory"), N_("Change the importance (nice value) of a process"),cb_save_visible, NULL, NULL, 0, 0, 0, GDK_CONTROL_MASK
        },
        {
          GNOME_APP_UI_TOGGLEITEM, N_("_Swap Space"), N_("Change the importance (nice value) of a process"),
         cb_save_visible, NULL, NULL, 0, 0,
         0, GDK_CONTROL_MASK
        },
        GNOMEUIINFO_SEPARATOR, 
        {
          GNOME_APP_UI_TOGGLEITEM, N_("_Time Started"), N_("Change the importance (nice value) of a process"),
         cb_save_visible, NULL, NULL, 0, 0,
         0, GDK_CONTROL_MASK
        },
        {
          GNOME_APP_UI_TOGGLEITEM, N_("_Parent "), N_("Change the importance (nice value) of a process"),
         cb_save_visible, NULL, NULL, 0, 0,
         0, GDK_CONTROL_MASK
        },
        {
          GNOME_APP_UI_TOGGLEITEM, N_("Co_mmand"), N_("Change the importance (nice value) of a process"),
         cb_save_visible, NULL, NULL, 0, 0,
         0, GDK_CONTROL_MASK
        },
        GNOMEUIINFO_END 
};
*/
static GnomeUIInfo sort_by_fields1_menu_uiinfo[] =
{
        {
          GNOME_APP_UI_ITEM, N_("Process I_D"), N_("Change the sorting order"), 
	  (gpointer)cb_sort_clicked_id, NULL, NULL, 0, 0, 0, GDK_CONTROL_MASK
        },
        {
          GNOME_APP_UI_ITEM, N_("_Name"), N_("Change the sorting order"),
         (gpointer)cb_sort_clicked_name, NULL, NULL, 0, 0,
         0, GDK_CONTROL_MASK
        }, 
        {
          GNOME_APP_UI_ITEM, N_("_Owner"), N_("Change the sorting order"),
         (gpointer)cb_sort_clicked_owner, NULL, NULL, 0, 0,
         0, GDK_CONTROL_MASK
        }, 
        {
          GNOME_APP_UI_ITEM, N_("_CPU %"), N_("Change the sorting order"),
         (gpointer)cb_sort_clicked_cpu, NULL, NULL, 0, 0,
         0, GDK_CONTROL_MASK
        }, 
        {
          GNOME_APP_UI_ITEM, N_("RAM"), N_("Change the sorting order"),
         (gpointer)cb_sort_clicked_mem, NULL, NULL, 0, 0,
         0, GDK_CONTROL_MASK
        }, 
        {
          GNOME_APP_UI_ITEM, N_("_Swap"), N_("Change the sorting order"),
         (gpointer)cb_sort_clicked_swap, NULL, NULL, 0, 0,
         0, GDK_CONTROL_MASK
        }, 
        {
          GNOME_APP_UI_ITEM, N_("Started"), N_("Change the sorting order"),
         (gpointer)cb_sort_clicked_time, NULL, NULL, 0, 0,
         0, GDK_CONTROL_MASK
        },
        {
          GNOME_APP_UI_ITEM, N_("_Parent "), N_("Change the sorting order"),
         (gpointer)cb_sort_clicked_parent, NULL, NULL, 0, 0,
         0, GDK_CONTROL_MASK
        }, 
        {
          GNOME_APP_UI_ITEM, N_("Co_mmand"), N_("Change the sorting order"),
        (gpointer) cb_sort_clicked_cmd, NULL, NULL, 0, 0,
         0, GDK_CONTROL_MASK
        },  
	GNOMEUIINFO_END
};
static GnomeUIInfo file1_menu_uiinfo[] =
{
        {
          GNOME_APP_UI_ITEM, N_("_Save Visible ..."), N_("Save the visible processes in a file"),
         (gpointer)cb_save_visible, NULL, NULL, 0, 0,
         's', GDK_CONTROL_MASK
        },
        {
          GNOME_APP_UI_ITEM, N_("_Log Visible..."), N_("Store the log of visible processes "),
         (gpointer)cb_log_visible, NULL, NULL, 0, 0,
         'l', GDK_CONTROL_MASK
        },	
        GNOMEUIINFO_SEPARATOR,
        {
          GNOME_APP_UI_ITEM, N_("_Send Signal..."), N_("Send a signal to selected process"),
         (gpointer)cb_send_signal, NULL, NULL, 0, 0,
         0, GDK_CONTROL_MASK
        },
        {
          GNOME_APP_UI_ITEM, N_("_Debug..."), N_("Debug the selected process"),
         (gpointer)cb_debug, NULL, NULL, 0, 0,
         'd', GDK_CONTROL_MASK
        },
        GNOMEUIINFO_SEPARATOR, 
        {
          GNOME_APP_UI_ITEM, N_("Trace _System Calls"), N_("Trace system calls of a process"),
         (gpointer)cb_trace_system_call, NULL, NULL, 0, 0,
         0, GDK_CONTROL_MASK
        }, 
        {
          GNOME_APP_UI_ITEM, N_("Trace _Children"), N_("Trace childrens of a process"),
         (gpointer)cb_trace_children, NULL, NULL, 0, 0,
         0, GDK_CONTROL_MASK
        },
        GNOMEUIINFO_SEPARATOR, 
        {
          GNOME_APP_UI_ITEM, N_("Sho_w Stack"), N_("Show stack of a selected process"),
         (gpointer)cb_show_stack, NULL, NULL, 0, 0,
         0, GDK_CONTROL_MASK
        }, 
        {
          GNOME_APP_UI_ITEM, N_("Show _Ancestry"), N_("Show ancestry of a selected process"),
         (gpointer)cb_show_ancestry, NULL, NULL, 0, 0,
         0, GDK_CONTROL_MASK
        },
        GNOMEUIINFO_SEPARATOR,
        {
          GNOME_APP_UI_ITEM, N_("_Kill Process"), N_("Kil the selected process"),
         (gpointer)cb_kill_process, NULL, NULL, 0, 0,
         'k', GDK_CONTROL_MASK
        },
        GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_EXIT_ITEM (cb_app_exit, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo edit1_menu_uiinfo[] =
{
 	{
 	  GNOME_APP_UI_ITEM, N_("_Copy"), N_("Copy the selected process in clipboard"),
	 (gpointer)cb_copy_clicked, NULL, NULL, 0, 0,
	 'c', GDK_CONTROL_MASK
	},
	{
	 GNOME_APP_UI_ITEM, N_("Select _All"), N_("Select All Process"),
	 (gpointer)cb_select_all, NULL, NULL, 0, 0,
	 'a', GDK_CONTROL_MASK
	}, 
	GNOMEUIINFO_END
};

static GnomeUIInfo view1_menu_uiinfo[] =
{
       {
         GNOME_APP_UI_SUBTREE, N_("So_rt By"), N_("Sort processes according to fields"),
         (gpointer)sort_by_fields1_menu_uiinfo, NULL, NULL, 0, 0,
         0, GDK_CONTROL_MASK
        },
        {
	GNOME_APP_UI_ITEM, N_("_Hide Process"), N_("Hide a selected process"),
          (gpointer)cb_hide_process, NULL, NULL, 0, 0,
         0, GDK_CONTROL_MASK
        },
        {
	GNOME_APP_UI_ITEM, N_("_Show Hidden Processes"), N_("View all hidden process"), (gpointer)cb_show_all_processes, NULL, NULL, 0, 0,
         0, GDK_CONTROL_MASK
        },
	GNOMEUIINFO_END
};

static GnomeUIInfo help1_menu_uiinfo[] =
{
	GNOMEUIINFO_HELP ("gprocview"),
	GNOMEUIINFO_MENU_ABOUT_ITEM (cb_about_activate, NULL),
	GNOMEUIINFO_END
};
static GnomeUIInfo sample1_menu_uiinfo[] =
{
             {
         GNOME_APP_UI_ITEM, N_("Sample _Now"), N_("Sample now "),
         (gpointer)cb_sample_now, NULL, NULL, 0, 0,
         'm', GDK_CONTROL_MASK
        }, 
        {
         GNOME_APP_UI_ITEM, N_("_Stop Sampling"), N_("Start or stop sampling"),
         (gpointer)cb_stop_sampling, NULL, NULL, 0, 0,
         't', GDK_CONTROL_MASK
        },  
	GNOMEUIINFO_END
};

static GnomeUIInfo menubar1_uiinfo[] =
{
	GNOMEUIINFO_MENU_FILE_TREE (file1_menu_uiinfo),
	GNOMEUIINFO_MENU_EDIT_TREE (edit1_menu_uiinfo),
	GNOMEUIINFO_MENU_VIEW_TREE (view1_menu_uiinfo),
	SAMPLE_TREE (sample1_menu_uiinfo),
	GNOMEUIINFO_MENU_HELP_TREE (help1_menu_uiinfo),
	GNOMEUIINFO_END
};

static GnomeUIInfo view_optionmenu[] = 
{
	{
 	  GNOME_APP_UI_ITEM, N_("All Processes"), N_("View processes being run by all users"),
	 (gpointer)cb_all_process_menu_clicked, NULL, NULL, 0, 0,
	 't', GDK_CONTROL_MASK
	},
	{
 	  GNOME_APP_UI_ITEM, N_("My Processes"), N_("View processes being run by you"),
	 (gpointer)cb_my_process_menu_clicked, NULL, NULL, 0, 0,
	 'p', GDK_CONTROL_MASK
	},
	GNOMEUIINFO_END
};

static GnomeUIInfo popup_menu_uiinfo[] =
{
        {
          GNOME_APP_UI_ITEM, N_("Copy"), N_("Copy the selected process"),
         (gpointer)cb_copy_clicked, NULL, NULL, 0, 0,
         'r', GDK_CONTROL_MASK
        },
        GNOMEUIINFO_SEPARATOR,
        {
          GNOME_APP_UI_ITEM, N_("Send Signal..."), N_("Send a signal to a selected process"),
         (gpointer)cb_send_signal, NULL, NULL, 0, 0,
         'r', GDK_CONTROL_MASK
        },
        {
          GNOME_APP_UI_ITEM, N_("Debug..."), N_("Debug the selected process"),
         (gpointer)cb_debug, NULL, NULL, 0, 0,
         'r', GDK_CONTROL_MASK
        },
        GNOMEUIINFO_SEPARATOR,
        {
          GNOME_APP_UI_ITEM, N_("Trace System Calls"), N_("Trace system calls of a process"), (gpointer)cb_trace_system_call, NULL, NULL, 0, 0, 'r', GDK_CONTROL_MASK
        },
        {
          GNOME_APP_UI_ITEM, N_("Trace Children"), N_("Trace children of a process"),
         (gpointer)cb_trace_children, NULL, NULL, 0, 0,
         'r', GDK_CONTROL_MASK
        },
        GNOMEUIINFO_SEPARATOR,
        {
          GNOME_APP_UI_ITEM, N_("Show Stack"), N_("Show stack of a selected process"),
         (gpointer)cb_show_stack, NULL, NULL, 0, 0,
         'r', GDK_CONTROL_MASK
        },
        {
          GNOME_APP_UI_ITEM, N_("Show Ancestry"), N_("Show ancestry of a process"), (gpointer)cb_show_ancestry, NULL, NULL, 0, 0, 'r', GDK_CONTROL_MASK
        },
        {
          GNOME_APP_UI_ITEM, N_("Kill Process"), N_("Kill the selected process"), (gpointer)popup_menu_kill_process, NULL, NULL, 0, 0,  'r', GDK_CONTROL_MASK
        },
	GNOMEUIINFO_END
};

gchar *moreinfolabel = N_("More _Info >>");
gchar *lessinfolabel = N_("<< Less _Info");

GtkWidget *infobutton;
GtkWidget *infolabel;
GtkWidget *popup_menu;
GtkWidget *sys_pane;

gint
get_sys_pane_pos (void)
{
	return GTK_PANED (sys_pane)->child1_size;
}


GtkWidget *scrolled;
GtkWidget *filter_entry;
static GtkWidget *
create_proc_view (ProcData *procdata)
{
	GtkWidget *vbox1;
	GtkWidget *hbox1;
	GtkWidget *search_label;
	GtkWidget *search_entry;
	GtkWidget *filter_label;
	//GtkWidget *filter_entry;
	GtkWidget *sample_time_spin;
	GtkWidget *sample_time_label;
	GtkWidget *sec_label;
	GtkWidget *show_label;
	GtkWidget *optionmenu1;
	GtkWidget *optionmenu1_menu;
	//GtkWidget *scrolled;
	GtkWidget *infobox;
	GtkWidget *label;
	GtkWidget *hbox2;
   	GtkAdjustment *adj; 	
	gfloat update;
	GList *items = NULL;
	vbox1 = gtk_vbox_new (FALSE, 0);
	
	hbox1 = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox1), hbox1, FALSE, FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (hbox1), GNOME_PAD_SMALL);

        filter_label = gtk_label_new_with_mnemonic (_("F_ilter:"));
        gtk_box_pack_start (GTK_BOX (hbox1), filter_label, FALSE, FALSE, 0);
        gtk_misc_set_padding (GTK_MISC (filter_label), GNOME_PAD_SMALL, 0);

       // update = (gfloat) procdata->config.update_interval;
 	adj = (GtkAdjustment *) gtk_adjustment_new (30.0, 1.0, 4294967.0, 1.0, 1.0, 1.0);
        filter_entry = gtk_entry_new ();
	gtk_label_set_mnemonic_widget (GTK_LABEL (filter_label), filter_entry);

        gtk_box_pack_start (GTK_BOX (hbox1), filter_entry, FALSE, TRUE, 10); 
        g_signal_connect (G_OBJECT (filter_entry), "activate",
                          G_CALLBACK (cb_filter), procdata);
	//sample_time_spin =  gtk_spin_button_new_with_range (1,(gdouble)MAX_INTERVAL,1);
	sample_time_spin =  gtk_spin_button_new (adj, 0.0,0);	
	gtk_spin_button_set_update_policy (GTK_SPIN_BUTTON (sample_time_spin), GTK_UPDATE_IF_VALID);	
	gtk_spin_button_set_value (GTK_SPIN_BUTTON(sample_time_spin), 30);
	procdata->config.update_interval = 30 * 1000;

	sample_time_label =  gtk_label_new_with_mnemonic (_("S_ample Every:"));  
	gtk_label_set_mnemonic_widget (GTK_LABEL (sample_time_label), sample_time_spin);

	gtk_box_pack_start (GTK_BOX (hbox1), sample_time_label, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox1), sample_time_spin, FALSE, FALSE, 0);
	sec_label =  gtk_label_new_with_mnemonic (_(" s"));  
	gtk_box_pack_start (GTK_BOX (hbox1), sec_label, FALSE, FALSE, 0);

	//g_object_set_data (G_OBJECT (sample_time_spin), "spin_button", spin_button);
        g_signal_connect (G_OBJECT (sample_time_spin), "value_changed",
                          G_CALLBACK (update_update_interval), procdata);
	//gtk_spin_button_set_value (GTK_SPIN_BUTTON(sample_time_spin), procdata->config.update_interval); 

	search_label = gtk_label_new_with_mnemonic (_("Fi_nd: "));
	gtk_box_pack_start (GTK_BOX (hbox1), search_label, FALSE, FALSE, 10);
	gtk_misc_set_padding (GTK_MISC (search_label), GNOME_PAD_SMALL, 0);

	search_entry = gtk_entry_new ();
	gtk_box_pack_start (GTK_BOX (hbox1), search_entry, FALSE, FALSE, 0);
	g_signal_connect (G_OBJECT (search_entry), "activate",
			  G_CALLBACK (cb_search), procdata);
	gtk_label_set_mnemonic_widget (GTK_LABEL (search_label), search_entry);
	
	optionmenu1 = gtk_option_menu_new ();
	gtk_box_pack_end (GTK_BOX (hbox1), optionmenu1, FALSE, FALSE, 0);
  	optionmenu1_menu = gtk_menu_new ();

  	gnome_app_fill_menu_with_data (GTK_MENU_SHELL (optionmenu1_menu), view_optionmenu,
  			               NULL, TRUE, 0, procdata);

	gtk_menu_set_active (GTK_MENU (optionmenu1_menu), procdata->config.whose_process);
  	gtk_option_menu_set_menu (GTK_OPTION_MENU (optionmenu1), optionmenu1_menu);
  	  	
  	label = gtk_label_new_with_mnemonic (_("Sh_ow:"));
  	gtk_label_set_mnemonic_widget (GTK_LABEL (label), optionmenu1);
	gtk_box_pack_end (GTK_BOX (hbox1), label, FALSE, FALSE, 0);
	gtk_misc_set_padding (GTK_MISC (label), GNOME_PAD_SMALL, 0);
	
	gtk_widget_show_all (hbox1);
	
	scrolled = proctable_new (procdata);
	if (!scrolled)
		return NULL;
	
	gtk_box_pack_start (GTK_BOX (vbox1), scrolled, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (scrolled), GNOME_PAD_SMALL);

	gail_up = check_gail(filter_entry);

	if (gail_up)
	{
		add_atk_namedesc(filter_entry, _("Filter"), _("Enter the filter string"));
		add_atk_namedesc(sample_time_spin, _("Sample"), _("Select sample time in seconds"));
		add_atk_namedesc(search_entry, _("Search"), _("Search for processes by name/process id etc"));
		add_atk_namedesc(optionmenu1, _("View"), _("Select the processes to view"));
		add_atk_relation(filter_entry, filter_label, ATK_RELATION_LABELLED_BY);
		add_atk_relation(sample_time_spin, sample_time_label, ATK_RELATION_LABELLED_BY);
		add_atk_relation(search_entry, search_label, ATK_RELATION_LABELLED_BY);
		add_atk_relation(optionmenu1, label, ATK_RELATION_LABELLED_BY);
     
		add_atk_relation(filter_entry, procdata->tree, ATK_RELATION_CONTROLLER_FOR);
		add_atk_relation(procdata->tree, filter_entry, ATK_RELATION_CONTROLLED_BY);
		add_atk_relation(search_entry, procdata->tree, ATK_RELATION_CONTROLLER_FOR);
		add_atk_relation(procdata->tree, search_entry, ATK_RELATION_CONTROLLED_BY);
		add_atk_relation(optionmenu1, procdata->tree, ATK_RELATION_CONTROLLER_FOR);
		add_atk_relation(procdata->tree, optionmenu1, ATK_RELATION_CONTROLLED_BY);

	}
	
	gtk_widget_show_all (scrolled);
/*	
	infobox = infoview_create (procdata);
	gtk_box_pack_start (GTK_BOX (vbox1), infobox, FALSE, FALSE, 0);

	hbox2 = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox1), hbox2, FALSE, FALSE, 0);
	
	endprocessbutton = gtk_button_new_with_mnemonic (_("End _Process"));
  	gtk_box_pack_end (GTK_BOX (hbox2), endprocessbutton, FALSE, FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (endprocessbutton), GNOME_PAD_SMALL);
	gtk_misc_set_padding (GTK_MISC (GTK_BIN (endprocessbutton)->child), 
			      GNOME_PAD_SMALL, 1);
	g_signal_connect (G_OBJECT (endprocessbutton), "clicked",
			  G_CALLBACK (cb_end_process_button_pressed), procdata);

	infolabel = gtk_label_new_with_mnemonic (_("More _Info"));
	infobutton = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (infobutton), infolabel);
	gtk_box_pack_start (GTK_BOX (hbox2), infobutton, FALSE, FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (infobutton), GNOME_PAD_SMALL);
	gtk_misc_set_padding (GTK_MISC (GTK_BIN (infobutton)->child), GNOME_PAD_SMALL, 1);
	g_signal_connect (G_OBJECT (infobutton), "clicked",
			  G_CALLBACK (cb_info_button_pressed), procdata);
	
	gtk_widget_show_all (hbox2);
*/	
	/* create popup_menu */
 	popup_menu = gtk_menu_new ();
 	gnome_app_fill_menu_with_data (GTK_MENU_SHELL (popup_menu), popup_menu_uiinfo,
  			               NULL, TRUE, 0, procdata);
	gtk_widget_show (popup_menu);
     
	g_signal_connect (procdata->tree, "popup_menu",
			 G_CALLBACK (cb_tree_popup_menu), popup_menu);
 
        return vbox1;
}

GtkWidget*
create_main_window (ProcData *procdata)
{
	gint width, height;
	GtkWidget *app;
	GtkWidget *notebook;
	GtkWidget *tab_label1, *tab_label2;
	GtkWidget *vbox1;
	GtkWidget *appbar1;
	GtkWidget *toolbar1;
	struct passwd *pwd;	
	gchar *gproc_name = NULL;
	gchar tempbuf [MAX_TEMP_BUFFER_SIZE] = {0};

      	pwd  = getpwuid (getuid ());
	sysinfo (SI_HOSTNAME, tempbuf, MAX_TEMP_BUFFER_SIZE);
	gproc_name = g_strdup_printf ("GProcView:%s@%s", pwd->pw_name, tempbuf);
	app = gnome_app_new ("gprocview", _(gproc_name));
 	g_free (gproc_name);	
	width = procdata->config.width;
	height = procdata->config.height;
	gtk_window_set_default_size (GTK_WINDOW (app), width, height);
	gtk_window_set_policy (GTK_WINDOW (app), FALSE, TRUE, TRUE);
	
	gnome_app_create_menus_with_data (GNOME_APP (app), menubar1_uiinfo, procdata);
	
	notebook = gtk_notebook_new ();
	gtk_container_set_border_width (GTK_CONTAINER (notebook), GNOME_PAD_SMALL);

	vbox1 = create_proc_view (procdata);
	appbar1 = gnome_appbar_new (FALSE, TRUE, GNOME_PREFERENCES_NEVER);
	gnome_app_set_statusbar (GNOME_APP (app), appbar1);
	
	//gnome_app_create_toolbar_with_data (GNOME_APP(app),toolbar,app); 	       

	g_signal_connect (G_OBJECT (app), "delete_event",
                          G_CALLBACK (cb_app_delete),
                          procdata);
	
	gnome_app_install_menu_hints (GNOME_APP (app), menubar1_uiinfo);
		    

	procdata->timeout = gtk_timeout_add (procdata->config.update_interval,
			 		     cb_timeout, procdata);
	 	
	gtk_widget_show (vbox1);
	//gnome_app_set_contents (GNOME_APP (app), notebook);
	gnome_app_set_contents (GNOME_APP (app), vbox1);
	//gtk_widget_show (notebook);

	update_sensitivity (procdata, FALSE);
	update_hide_process_menu(procdata);
	
	/* We cheat and force it to set up the labels */
 	procdata->config.show_more_info = !procdata->config.show_more_info;
 	//toggle_infoview (procdata);

 	//gtk_notebook_set_current_page (GTK_NOTEBOOK (notebook), procdata->config.current_tab);
	
 	return app;

}

GtkWidget*
create_simple_view_dialog (ProcData *procdata)
{
#if 0
	GtkWidget *app = NULL;
	GtkWidget *main_vbox;
	GtkWidget *scrolled;
	GtkWidget *vbox;
	GtkWidget *hbox;
	GtkWidget *label;
	GtkWidget *button;
	GtkWidget *frame;
	guint key;
	
	app = gnome_dialog_new (_("Application Manager"), GNOME_STOCK_BUTTON_CANCEL, NULL);
	gtk_window_set_policy (GTK_WINDOW (app), FALSE, TRUE, FALSE);
	gtk_window_set_default_size (GTK_WINDOW (app), 350, 425);
	
	accel = gtk_accel_group_new ();
	gtk_accel_group_attach (accel, G_OBJECT (app));
	gtk_accel_group_unref (accel);
	
	main_vbox = GNOME_DIALOG (app)->vbox;
	
	frame = gtk_frame_new (_("Running Applications"));
	gtk_container_set_border_width (GTK_CONTAINER (frame), GNOME_PAD_SMALL);
	gtk_box_pack_start (GTK_BOX (main_vbox), frame, TRUE, TRUE, 0);
	
	vbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), GNOME_PAD_SMALL);
	gtk_container_add (GTK_CONTAINER (frame), vbox);
	
	scrolled = proctable_new (procdata);
	if (!scrolled)
		return NULL;
	gtk_box_pack_start (GTK_BOX (vbox), scrolled, TRUE, TRUE, 0);
	
	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	
	button = gtk_button_new ();
	endprocessbutton = button;
  	label = gtk_label_new (NULL);
	key = gtk_label_parse_uline (GTK_LABEL (label), _("_Close Application"));
	gtk_widget_add_accelerator (button, "clicked",
				    accel,
				    key,
				    GDK_MOD1_MASK,
				    0);
	gtk_container_add (GTK_CONTAINER (button), label);
	gtk_box_pack_end (GTK_BOX (hbox), button, FALSE, FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (button), GNOME_PAD_SMALL);
	gtk_misc_set_padding (GTK_MISC (GTK_BIN (button)->child), 
			      GNOME_PAD_SMALL, 1);
	g_signal_connect (G_OBJECT (button), "clicked",
			    G_CALLBACK (cb_end_process_button_pressed), procdata);

	g_signal_connect (G_OBJECT (app), "close",
			    G_CALLBACK (cb_close_simple_dialog), procdata);
	g_signal_connect (G_OBJECT (procdata->tree), "cursor_activated",
			    G_CALLBACK (cb_table_selected), procdata);
			    
	/* Makes sure everything that should be insensitive is at start */
	gtk_signal_emit_by_name (G_OBJECT (procdata->tree), "cursor_activated",
				 -1, NULL);
				 
	procdata->timeout = gtk_timeout_add (procdata->config.update_interval,
			 		     cb_timeout, procdata);
			 		    
	
			    
	gtk_widget_show_all (main_vbox);
	gtk_widget_show (app);
	
	return app;
#endif
	return NULL;
}

void
toggle_infoview (ProcData *data)
{
	ProcData *procdata = data;
	GtkWidget *label;
	
	label = infolabel;
		
	if (procdata->config.show_more_info == FALSE)
	{
		//infoview_update (procdata);
		gtk_widget_show_all (procdata->infobox);
		procdata->config.show_more_info = TRUE;	
		gtk_label_set_text_with_mnemonic (GTK_LABEL (label),_(lessinfolabel)); 
 				
	}			
	else
	{
		gtk_widget_hide (procdata->infobox);
		procdata->config.show_more_info = FALSE;
 		gtk_label_set_text_with_mnemonic (GTK_LABEL (label),_(moreinfolabel)); 
	}
}

void do_popup_menu (ProcData *data, GdkEventButton *event)
{

	if (event->type == GDK_BUTTON_PRESS)
        {
                if (event->button == 3)
                {
                	gtk_menu_popup (GTK_MENU (popup_menu), NULL, NULL,
                                        NULL, NULL, event->button,
                                        event->time);
                }
        }
}

void 
update_hide_process_menu(ProcData *data)
{
	if (procdata->blacklist_num == 0 )
		gtk_widget_set_sensitive (view1_menu_uiinfo[2].widget, FALSE);
	else
		gtk_widget_set_sensitive (view1_menu_uiinfo[2].widget, TRUE);
		
}

void
update_sensitivity (ProcData *data, gboolean sensitivity)
{
//	gtk_widget_set_sensitive (endprocessbutton, sensitivity);
	
	if (!data->config.simple_view) {
		//gtk_widget_set_sensitive (data->infobox, sensitivity);
		//gtk_widget_set_sensitive (edit1_menu_uiinfo[0].widget, sensitivity);
		//gtk_widget_set_sensitive (edit1_menu_uiinfo[1].widget, sensitivity);
		//gtk_widget_set_sensitive (view1_menu_uiinfo[0].widget, sensitivity);
		//gtk_widget_set_sensitive (edit1_menu_uiinfo[2].widget, sensitivity);
		//gtk_widget_set_sensitive (edit1_menu_uiinfo[3].widget, sensitivity);
		//gtk_widget_set_sensitive (edit1_menu_uiinfo[4].widget, sensitivity);



		/*  To Disable/Unable Hide menu item  */
		gtk_widget_set_sensitive (view1_menu_uiinfo[1].widget, sensitivity);
		gtk_widget_set_sensitive (edit1_menu_uiinfo[0].widget, sensitivity);
		
		/* To Disable/Unable menu items under file menu option  */			
		//gtk_widget_set_sensitive (file1_menu_uiinfo[0].widget, sensitivity);
		//gtk_widget_set_sensitive (file1_menu_uiinfo[1].widget, sensitivity);
		gtk_widget_set_sensitive (file1_menu_uiinfo[2].widget, sensitivity);
		gtk_widget_set_sensitive (file1_menu_uiinfo[3].widget, sensitivity);
		gtk_widget_set_sensitive (file1_menu_uiinfo[4].widget, sensitivity);
		gtk_widget_set_sensitive (file1_menu_uiinfo[5].widget, sensitivity);
		gtk_widget_set_sensitive (file1_menu_uiinfo[6].widget, sensitivity);
		gtk_widget_set_sensitive (file1_menu_uiinfo[7].widget, sensitivity);
		gtk_widget_set_sensitive (file1_menu_uiinfo[8].widget, sensitivity);
		gtk_widget_set_sensitive (file1_menu_uiinfo[9].widget, sensitivity);
		gtk_widget_set_sensitive (file1_menu_uiinfo[10].widget, sensitivity);
		gtk_widget_set_sensitive (file1_menu_uiinfo[11].widget, sensitivity);
		gtk_widget_set_sensitive (file1_menu_uiinfo[12].widget, sensitivity);

		//gtk_widget_set_sensitive (view_optionmenu[0].widget, sensitivity);
		//gtk_widget_set_sensitive (view_optionmenu[1].widget, sensitivity);
	}
}	

void
update_sensitivity_controls (gboolean sensitivity)
{
	gtk_widget_set_sensitive (filter_entry, sensitivity);
	if (isStopSamplingSelected == FALSE) {
		gtk_widget_set_sensitive (view_optionmenu[0].widget, sensitivity);
		gtk_widget_set_sensitive (view_optionmenu[1].widget, sensitivity);
	}
	gtk_widget_set_sensitive (sample1_menu_uiinfo[0].widget, sensitivity);
//	gtk_widget_set_sensitive (scrolled, sensitivity);
}

void
update_sensitivity_process (gboolean sensitivity)
{
	gtk_widget_set_sensitive (view_optionmenu[0].widget, sensitivity);
	gtk_widget_set_sensitive (view_optionmenu[1].widget, sensitivity);
}
